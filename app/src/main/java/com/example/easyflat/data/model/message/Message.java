package com.example.easyflat.data.model.message;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.Date;

@Entity(foreignKeys = {@ForeignKey(entity = Group.class,
        parentColumns = "id",
        childColumns = "group_id"),
        @ForeignKey(entity = User.class,
                parentColumns = "id",
                childColumns = "user_id")}, indices = {@Index("user_id"), @Index("group_id")})
public class Message {

    @Ignore
    public static final String TAG = "Message";
    //region Variables

    @PrimaryKey
    @NonNull
    private int id;
    @NonNull
    private int group_id;
    @NonNull
    private String message;
    @NonNull
    private int user_id;
    @NonNull
    private String created_at;
    //endregion

    //region Getter and Setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    @NonNull
    public String getMessage() {
        return message;
    }

    public void setMessage(@NonNull String message) {
        this.message = message;
    }

    @NonNull
    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(@NonNull int user_id) {
        this.user_id = user_id;
    }

    @NonNull
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(@NonNull String created_at) {
        this.created_at = created_at;
    }

    //endregion


    public Message(int group_id, @NonNull String message, @NonNull int user_id, @NonNull String created_at) {
        this.group_id = group_id;
        this.message = message;
        this.user_id = user_id;
        this.created_at = created_at;
    }
}
