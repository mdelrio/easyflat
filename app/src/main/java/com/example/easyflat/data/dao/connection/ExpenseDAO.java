package com.example.easyflat.data.dao.connection;

import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.model.expense.ExpenseResponse;
import com.example.easyflat.data.network.ApiTokenRestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpenseDAO {

    public interface ExpensesGetDAOListener{
        void onSuccessExpensesGet(List<Expense> expenses);
        void onErrorExpensesGet(String error);
        void onSuccessDelete(Expense expense);
        void onErrorDelete(String error);
    }

    public interface ExpensesManageDAOListener {
        void onSuccessAddExpense(Expense expense);
        void onErrorAddExpense(String error);
        void onSuccessEdit(Expense expense);
        void onErrorEdit(String error);
    }

    public static void getExpenses(final int group_id, final ExpensesGetDAOListener expensesGetDAOListener) {
        Call<ExpenseResponse> call = ApiTokenRestClient.getInstance().getExpenses(group_id);
        call.enqueue(new Callback<ExpenseResponse>() {

            @Override
            public void onResponse(Call<ExpenseResponse> call, Response<ExpenseResponse> response) {
                if (response.isSuccessful()) {
                    expensesGetDAOListener.onSuccessExpensesGet(response.body().getExpenses());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            expensesGetDAOListener.onErrorExpensesGet(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ExpenseResponse> call, Throwable t) {
                expensesGetDAOListener.onErrorExpensesGet(t.getMessage());
            }
        });
    }

    public static void addExpense(final String users_id, final int group_id, final String subject, final String description, final float price, final ExpensesManageDAOListener expensesManageDAOListener) {
        Call<ExpenseResponse> call = ApiTokenRestClient.getInstance().addExpense(users_id, group_id, subject, description, price);
        call.enqueue(new Callback<ExpenseResponse>() {

            @Override
            public void onResponse(Call<ExpenseResponse> call, Response<ExpenseResponse> response) {
                if (response.isSuccessful()) {
                    expensesManageDAOListener.onSuccessAddExpense(response.body().getExpense());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            expensesManageDAOListener.onErrorAddExpense(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ExpenseResponse> call, Throwable t) {
                expensesManageDAOListener.onErrorAddExpense(t.getMessage());
            }
        });
    }

    public static void editExpense(final int id, String usersId, String subject, String description, float price, final ExpensesManageDAOListener expensesManageDAOListener) {
        Call<ExpenseResponse> call = ApiTokenRestClient.getInstance().editExpense(id, usersId, subject, description, price);
        call.enqueue(new Callback<ExpenseResponse>() {

            @Override
            public void onResponse(Call<ExpenseResponse> call, Response<ExpenseResponse> response) {
                if (response.isSuccessful()) {
                    expensesManageDAOListener.onSuccessEdit(response.body().getExpense());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            expensesManageDAOListener.onErrorEdit(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ExpenseResponse> call, Throwable t) {
                expensesManageDAOListener.onErrorAddExpense(t.getMessage());
            }
        });
    }

    public static void deleteExpense(final int id, final ExpensesGetDAOListener expensesManageDAOListener) {
        Call<ExpenseResponse> call = ApiTokenRestClient.getInstance().deleteExpense(id);
        call.enqueue(new Callback<ExpenseResponse>() {

            @Override
            public void onResponse(Call<ExpenseResponse> call, Response<ExpenseResponse> response) {
                if (response.isSuccessful()) {
                    expensesManageDAOListener.onSuccessDelete(response.body().getExpense());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            expensesManageDAOListener.onErrorDelete(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ExpenseResponse> call, Throwable t) {
                expensesManageDAOListener.onErrorDelete(t.getMessage());
            }
        });
    }
}
