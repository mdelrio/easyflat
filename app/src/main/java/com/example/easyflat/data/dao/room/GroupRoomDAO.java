package com.example.easyflat.data.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.easyflat.data.model.group.Group;

import java.util.List;

@Dao
public interface GroupRoomDAO {
        @Query("SELECT * FROM `Group`")
        List<Group> getGroups();

        @Insert
        long addGroup(Group group);

        @Query("DELETE FROM `Group` WHERE id = :id")
        void deleteGroup(int id);

        @Insert
        void addGroups(List<Group> groups);

        @Query("SELECT * FROM `Group` WHERE id = :groupid")
        Group getGroup(int groupid);

        @Delete
        void deleteGroup(Group group);
}
