package com.example.easyflat.data.dao.connection;

import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.model.group.GroupResponse;
import com.example.easyflat.data.network.ApiTokenRestClient;
import com.example.easyflat.data.repository.GroupRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupDAO {

    public interface CreateGroupDAOListener {
        void onSuccessCreated(Group group);
        void onErrorCreated(String error);
    }

    public interface JoinGroupDAOListener {
        void onSuccessJoined(Group group);
        void onErrorJoined(String error);
    }

    public interface ExitGroupDAOListener {
        void onSuccessExit(int id);
        void onErrorExit(String error);
    }

    public interface GetGroupsDAOListener {
        void onSuccessGroupsGet(List<Group> groups);
        void onErrorGroupsGet(String error);
    }


    public static void createGroup(final String name, final CreateGroupDAOListener createGroupDAOListener) {
        Call<GroupResponse> call = ApiTokenRestClient.getInstance().createGroup(name);
        call.enqueue(new Callback<GroupResponse>() {

            @Override
            public void onResponse(Call<GroupResponse> call, Response<GroupResponse> response) {
                if (response.isSuccessful()) {
                    createGroupDAOListener.onSuccessCreated(response.body().getGroup());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            createGroupDAOListener.onErrorCreated(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<GroupResponse> call, Throwable t) {
                createGroupDAOListener.onErrorCreated(t.getMessage());
            }
        });
    }

    public static void getGroups(final GetGroupsDAOListener getGroupsDAOListener) {
        Call<GroupResponse> call = ApiTokenRestClient.getInstance().getGroups();
        call.enqueue(new Callback<GroupResponse>() {

            @Override
            public void onResponse(Call<GroupResponse> call, Response<GroupResponse> response) {
                if (response.isSuccessful()) {
                    getGroupsDAOListener.onSuccessGroupsGet(response.body().getGroups());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            getGroupsDAOListener.onErrorGroupsGet(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<GroupResponse> call, Throwable t) {
                getGroupsDAOListener.onErrorGroupsGet(t.getMessage());
            }
        });
    }

    public static void joinGroup(final String code, final JoinGroupDAOListener joinGroupDAOListener) {
        Call<GroupResponse> call = ApiTokenRestClient.getInstance().joinGroup(code);
        call.enqueue(new Callback<GroupResponse>() {

            @Override
            public void onResponse(Call<GroupResponse> call, Response<GroupResponse> response) {
                if (response.isSuccessful()) {
                    joinGroupDAOListener.onSuccessJoined(response.body().getGroup());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            joinGroupDAOListener.onErrorJoined(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<GroupResponse> call, Throwable t) {
                joinGroupDAOListener.onErrorJoined(t.getMessage());
            }
        });
    }

    public static void exitGroup(final int id, final ExitGroupDAOListener exitGroupDAOListener) {
        Call<GroupResponse> call = ApiTokenRestClient.getInstance().exitGroup(id);
        call.enqueue(new Callback<GroupResponse>() {

            @Override
            public void onResponse(Call<GroupResponse> call, Response<GroupResponse> response) {
                if (response.isSuccessful()) {
                    exitGroupDAOListener.onSuccessExit(response.body().getId());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            exitGroupDAOListener.onErrorExit(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<GroupResponse> call, Throwable t) {
                exitGroupDAOListener.onErrorExit(t.getMessage());
            }
        });
    }
}
