package com.example.easyflat.data.repository;

import com.example.easyflat.data.dao.connection.GroupDAO;
import com.example.easyflat.data.dao.connection.UserDAO;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.dao.room.GroupRoomDAO;
import com.example.easyflat.data.dao.room.UserRoomDAO;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.group.Group;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GroupRepository {
    private static GroupRepository groupRepository;
    private GroupRoomDAO groupRoomDAO;



    static {
        groupRepository = new GroupRepository();
    }

    private GroupRepository() {
        groupRoomDAO = EasyFlatDatabase.getDatabase().groupRoomDAO();
    }

    public static GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public void createGroup(String name, GroupDAO.CreateGroupDAOListener createGroupDAOListener) {
        GroupDAO.createGroup(name, createGroupDAOListener);
    }

    public void downloadGroups(GroupDAO.GetGroupsDAOListener getGroupsDAOListener){
        GroupDAO.getGroups(getGroupsDAOListener);
    }

    public void desuscribeGroup(int group_id, GroupDAO.ExitGroupDAOListener exitGroupDAOListener){
        GroupDAO.exitGroup(group_id, exitGroupDAOListener);
    }

    public List<Group> getGroups(){
        List<Group> groups = new ArrayList<>();
        try {
            groups =  EasyFlatDatabase.databaseWriteExecutor.submit(() -> groupRoomDAO.getGroups()).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return groups;
    }

    public void joinGroup(final String code, GroupDAO.JoinGroupDAOListener joinGroupDAOListener){
        GroupDAO.joinGroup(code, joinGroupDAOListener);
    }

    public long addGroup(final Group group) {
        long rowId = -1;
        try {
            rowId = EasyFlatDatabase.databaseWriteExecutor.submit(() -> groupRoomDAO.addGroup(group)).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return rowId;
    }

    public boolean deleteGroup(int id) {
        EasyFlatDatabase.databaseWriteExecutor.submit(() -> groupRoomDAO.deleteGroup(id));
        return true;
    }

    public void addAll(List<Group> groups) {
        EasyFlatDatabase.databaseWriteExecutor.submit(() -> groupRoomDAO.addGroups(groups));
    }

    public Group getGroup(int groupid) {
        Group group = null;
        try {
            group = EasyFlatDatabase.databaseWriteExecutor.submit(()-> groupRoomDAO.getGroup(groupid)).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return group;
    }
}
