package com.example.easyflat.data.dao.connection;

import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.login.LoginResponse;
import com.example.easyflat.data.model.login.ProfileResponse;
import com.example.easyflat.data.model.login.RecoverPasswordResponse;
import com.example.easyflat.data.model.login.RegisterResponse;
import com.example.easyflat.data.network.ApiRestClient;
import com.example.easyflat.data.network.ApiTokenRestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginDAO {

    public interface ResponseLoginApi {
        void onSuccessSignIn(String token);
        void onErrorLogin(String error);
    }

    public interface ResponseProfileApi {
        void onSuccessUser(User user);
        void onErrorUser(String error);
    }

    public interface ResponseRegisterApi {
        void onSuccessRegister(String message);
        void onErrorRegister(String error);
    }

    public interface ResponseRecoverPasswordApi {
        void onSuccessSend(String message);
        void onErrorSend(String error);
    }


    public static void signUpServer(final String name, final String email, final String password, final ResponseRegisterApi responseRegisterApi) {
        Call<RegisterResponse> call = ApiRestClient.getInstance().register(name, email, password);
        call.enqueue(new Callback<RegisterResponse>() {

            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.isSuccessful()) {
                    responseRegisterApi.onSuccessRegister(response.body().getMessage());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("email") != null)
                            responseRegisterApi.onErrorRegister(errorObject.getJSONObject("message").getString("email"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                responseRegisterApi.onErrorRegister(t.getMessage());
            }
        });
    }

    public static void signInServer(final String email, final String password, final ResponseLoginApi responseLoginApi) {
        Call<LoginResponse> call = ApiRestClient.getInstance().login(email, password);
        call.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    responseLoginApi.onSuccessSignIn(response.body().getToken());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        responseLoginApi.onErrorLogin(errorObject.getString("error"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        responseLoginApi.onErrorLogin(e.getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                responseLoginApi.onErrorLogin(t.getMessage());
            }
        });
    }

    public static void getUser(final ResponseProfileApi responseProfileApi) {
        Call<ProfileResponse> call = ApiTokenRestClient.getInstance().getUser();
        call.enqueue(new Callback<ProfileResponse>() {

            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful()) {
                    responseProfileApi.onSuccessUser(response.body().getUser());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        responseProfileApi.onErrorUser(errorObject.getString("error"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        responseProfileApi.onErrorUser(e.getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                responseProfileApi.onErrorUser(t.getMessage());
            }
        });
    }

    public static void recoverPassword(final String email, final ResponseRecoverPasswordApi responseRecoverPasswordApi) {
        Call<RecoverPasswordResponse> call = ApiRestClient.getInstance().recoverPassword(email);
        call.enqueue(new Callback<RecoverPasswordResponse>() {

            @Override
            public void onResponse(Call<RecoverPasswordResponse> call, Response<RecoverPasswordResponse> response) {
                if (response.isSuccessful()) {
                    responseRecoverPasswordApi.onSuccessSend(response.body().getMessage());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("email") != null)
                            responseRecoverPasswordApi.onErrorSend(errorObject.getJSONObject("message").getString("email"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<RecoverPasswordResponse> call, Throwable t) {
                responseRecoverPasswordApi.onErrorSend(t.getMessage());
            }
        });
    }
}
