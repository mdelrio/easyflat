package com.example.easyflat.data.repository;

import com.example.easyflat.data.dao.connection.DebtDAO;
import com.example.easyflat.data.dao.room.DebtRoomDAO;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.model.debt.Debt;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class DebtRepository {
    private static DebtRepository debtRepository ;
    private DebtRoomDAO debtRoomDAO;

    static {
        debtRepository = new DebtRepository();
    }

    private Future<?> insert;

    private DebtRepository() {
        debtRoomDAO = EasyFlatDatabase.getDatabase().debtRoomDAO();
    }

    public static DebtRepository getDebtRepository() {
        return debtRepository;
    }

    public void downloadDebts(int group_id, DebtDAO.GetDebtsListener getDebtsListener){
        DebtDAO.getDebts(group_id, getDebtsListener);
    }

    public List<Debt> getDebts(){
        List<Debt> debts = new ArrayList<>();
        try {
            while (!insert.isDone()) {
                debts = EasyFlatDatabase.databaseWriteExecutor.submit(() -> debtRoomDAO.getUsersTotalPrice()).get();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return debts;
    }

    public void addAll(List<Debt> debts) {
        insert = EasyFlatDatabase.databaseWriteExecutor.submit(() -> debtRoomDAO.insertAll(debts));
        new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public List<Debt> getDebtExpense(int expense_id){
        List<Debt> debts = new ArrayList<>();
        try {
                debts = EasyFlatDatabase.databaseWriteExecutor.submit(() -> debtRoomDAO.getDebtsExpense(expense_id)).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return debts;
    }

    public void deleteDebts(){
        EasyFlatDatabase.databaseWriteExecutor.submit(() -> debtRoomDAO.deleteDebts());
    }
}
