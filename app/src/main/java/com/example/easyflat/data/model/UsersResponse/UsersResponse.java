package com.example.easyflat.data.model.UsersResponse;

import com.example.easyflat.data.model.User;

import java.util.List;

public class UsersResponse {
    private List<User> users;
    private String error;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
