package com.example.easyflat.data.adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easyflat.R;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.ArrayList;
import java.util.List;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {

    public interface OnclickItemListener {
        void change(Group group);

        void getInfo(Group group);
    }

    private OnclickItemListener onclickItemListener;
    private List<Group> groups;

    public GroupAdapter(OnclickItemListener onclickItemListener) {
        this.groups = new ArrayList<>();
        this.onclickItemListener = onclickItemListener;
    }

    public GroupAdapter() {
        this.groups = new ArrayList<>();
    }


    @NonNull
    @Override
    public GroupAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.group_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GroupAdapter.ViewHolder holder, int position) {
        if (EasyFlatApplication.getGroup().getId() == groups.get(position).getId())
            holder.tvName.setTypeface(null, Typeface.BOLD);

        holder.tvName.setText(groups.get(position).getName());
        holder.bind(groups.get(position), onclickItemListener);
    }

    public void addAll(List<Group> groups) {
        this.groups.addAll(groups);
    }

    public void clear() {
        groups.clear();
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvitGroupName);
        }

        public void bind(Group group, OnclickItemListener onclickItemListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (EasyFlatApplication.getGroup().getId() != group.getId())
                        onclickItemListener.change(group);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onclickItemListener.getInfo(group);
                    return true;
                }
            });
        }
    }
}
