package com.example.easyflat.data.model.expense;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.Calendar;
import java.util.Date;

@Entity(foreignKeys = {@ForeignKey(entity = Group.class,
        parentColumns = "id",
        childColumns = "group_id")}, indices = {@Index("group_id")})
public class Expense implements Parcelable {
    @PrimaryKey
    @NonNull
    private int id;
    @NonNull
    private int group_id;
    @NonNull
    private String subject;
    private String description;
    @NonNull
    private float price;
    private String image;
    @NonNull
    private String created_at;

    public Expense(String subject, String description, float price, String image) {
        this.id = -1;
        this.group_id = EasyFlatApplication.getGroup().getId();
        this.subject = subject;
        this.description = description;
        this.price = price;
        this.image = image;
    }

    protected Expense(Parcel in) {
        id = in.readInt();
        group_id = in.readInt();
        subject = in.readString();
        description = in.readString();
        price = in.readFloat();
        image = in.readString();
        created_at = in.readString();
    }

    public static final Creator<Expense> CREATOR = new Creator<Expense>() {
        @Override
        public Expense createFromParcel(Parcel in) {
            return new Expense(in);
        }

        @Override
        public Expense[] newArray(int size) {
            return new Expense[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @NonNull
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(@NonNull String created_at) {
        this.created_at = created_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(group_id);
        dest.writeString(subject);
        dest.writeString(description);
        dest.writeFloat(price);
        dest.writeString(image);
        dest.writeString(created_at);
    }
}
