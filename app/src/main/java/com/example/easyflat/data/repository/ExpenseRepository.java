package com.example.easyflat.data.repository;

import com.example.easyflat.data.dao.connection.ExpenseDAO;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.dao.room.ExpenseRoomDAO;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.expense.Expense;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ExpenseRepository {
    private static ExpenseRepository expenseRepository ;
    private ExpenseRoomDAO expenseRoomDAO;

    static {
        expenseRepository = new ExpenseRepository();
    }

    private ExpenseRepository() {
        expenseRoomDAO = EasyFlatDatabase.getDatabase().expenseRoomDAO();
    }

    public static ExpenseRepository getExpenseRepository() {
        return expenseRepository;
    }

    public void downloadExpenses(int group_id, ExpenseDAO.ExpensesGetDAOListener expensesGetDAOListener){
        ExpenseDAO.getExpenses(group_id, expensesGetDAOListener);
    }

    public void addExpense(String users_id, int group_id, String subject, String description, float price,  ExpenseDAO.ExpensesManageDAOListener expensesManageDAOListener){
        ExpenseDAO.addExpense(users_id, group_id, subject, description, price, expensesManageDAOListener);
    }

    public void editExpense(int id, String users_id, String subject, String description, float price, ExpenseDAO.ExpensesManageDAOListener expensesManageDAOListener){
        ExpenseDAO.editExpense(id, users_id, subject, description, price, expensesManageDAOListener);
    }

    public void deleteExpense(int id, ExpenseDAO.ExpensesGetDAOListener expensesManageDAOListener){
        ExpenseDAO.deleteExpense(id, expensesManageDAOListener);
    }

    public void deleteRoomExpense(int id){
        EasyFlatDatabase.databaseWriteExecutor.submit(() -> expenseRoomDAO.delete(id));
    }

    private List<Expense> getExpenses(){
        List<Expense> expenses = new ArrayList<>();
        try {
            expenses =  EasyFlatDatabase.databaseWriteExecutor.submit(() -> expenseRoomDAO.getExpenses()).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return expenses;
    }

    public long addExpense(final Expense expense) {
        long rowId = -1;
        try {
            rowId = EasyFlatDatabase.databaseWriteExecutor.submit(() -> expenseRoomDAO.addExpense(expense)).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return rowId;
    }

    public void addAll(List<Expense> expenses) {
        EasyFlatDatabase.databaseWriteExecutor.submit(() -> expenseRoomDAO.insertAll(expenses));
    }

    public List<Debt> getDebts(int expenseId) {
        return null;
    }

}
