package com.example.easyflat.data.dao.room;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.model.message.Message;
import com.example.easyflat.data.model.User;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {User.class, Group.class, Message.class, Debt.class, Expense.class}, version = 1, exportSchema = false)
public abstract class EasyFlatDatabase extends RoomDatabase {

    public abstract UserRoomDAO userRoomDAO();
    public abstract GroupRoomDAO groupRoomDAO();
    public abstract ChatRoomDAO chatRoomDAO();
    public abstract ExpenseRoomDAO expenseRoomDAO();
    public abstract DebtRoomDAO debtRoomDAO();

    private static volatile EasyFlatDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static EasyFlatDatabase getDatabase() {
        if (INSTANCE == null) {
            synchronized (EasyFlatDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(EasyFlatApplication.getAppContext(),
                            EasyFlatDatabase.class, "easyflat_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static void dropDatabase(){
        INSTANCE = null;
    }
}
