package com.example.easyflat.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easyflat.R;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.repository.UserRepository;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

public class PayForAdapter extends RecyclerView.Adapter<PayForAdapter.PayForHolder> {
    private Context context;
    private ArrayList<Debt> debts;
    private float quantity;

    public PayForAdapter(Context context) {
        this.context = context;
        this.debts = new ArrayList<>();
        this.quantity = 0;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
        updateDebts();
    }

    @NonNull
    @Override
    public PayForHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new PayForAdapter.PayForHolder(layoutInflater.inflate(R.layout.pay_for_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PayForHolder holder, int position) {
        Debt debt = debts.get(position);
        int marginLeft = context.getResources().getDimensionPixelOffset(R.dimen.card_margin);
        int marginTop = 0;
        int marginRight = context.getResources().getDimensionPixelOffset(R.dimen.card_margin);
        int marginBottom = context.getResources().getDimensionPixelOffset(R.dimen.card_margin);
        if (position <= 1) {
            marginTop = context.getResources().getDimensionPixelOffset(R.dimen.card_margin);
        }
        if (position % 2 == 0) {
            marginRight = 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.itemView.findViewById(R.id.cvPayFor).getLayoutParams();
        layoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        holder.cbName.setText(UserRepository.getUserRepository().getUser(debt.getUser_id()).getName());
        holder.tvQuantity.setText(String.format(CommonUtils.QUANTITY_FORMAT, debt.getPrice()));
        holder.cbName.setChecked(debt.isChecked());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                debt.setChecked(!debt.isChecked());
                holder.cbName.setChecked(debts.get(position).isChecked());
                updateDebts();
            }
        });
    }

    @Override
    public int getItemCount() {
        return debts.size();
    }

    public void addAll(Collection<Debt> debts) {
        this.debts.clear();
        this.debts.addAll(debts);
        notifyDataSetChanged();
    }

    public float getQuantity() {
        return quantity;
    }

    public ArrayList<Debt> getDebts() {
        return debts;
    }

    public class PayForHolder extends RecyclerView.ViewHolder {
        CheckBox cbName;
        TextView tvQuantity;

        public PayForHolder(@NonNull View itemView) {
            super(itemView);
            cbName = itemView.findViewById(R.id.cbName);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
        }
    }

    private void updateDebts() {
        int divider = 0;
        for (Debt debtItem : debts) {
            if (debtItem.isChecked()) {
                divider++;
            }
        }
        for (Debt debtItem : debts) {
            if (debtItem.isChecked()) {
                debtItem.setPrice(quantity / (float) divider);
            } else {
                debtItem.setPrice(0);
            }
        }
        notifyDataSetChanged();
    }
}

