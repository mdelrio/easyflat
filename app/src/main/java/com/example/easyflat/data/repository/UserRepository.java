package com.example.easyflat.data.repository;

import com.example.easyflat.data.dao.connection.LoginDAO;
import com.example.easyflat.data.dao.connection.UserDAO;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.dao.room.UserRoomDAO;
import com.example.easyflat.data.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UserRepository implements LoginDAO.ResponseLoginApi, LoginDAO.ResponseProfileApi, LoginDAO.ResponseRegisterApi {
    private static UserRepository userRepository;
    private UserRoomDAO userRoomDAO;

    private RepositorySignInListener repositorySignInListener;
    private RepositoryRegisterListener repositoryRegisterListener;
    private RepositoryGetUserListener repositoryGetUserListener;

    public interface RepositorySignInListener {
        void onSuccessSignIn(String token);
        void onErrorLogin(String error);
    }

    public interface RepositoryGetUserListener {
        void onSuccessGetUser(User user);
        void onErrorGetUser(String error);
    }

    public interface RepositoryRegisterListener {
        void onSuccessRegister(String message);
        void onErrorRegister(String error);
    }

    public void getUser(RepositoryGetUserListener repositoryGetUserListener) {
        this.repositoryGetUserListener = repositoryGetUserListener;
        LoginDAO.getUser(this);
    }

    public void downloadUsers(int group_id, UserDAO.ResponseListUsersApi responseListUsersApi){
        UserDAO.getUsers(group_id, responseListUsersApi);
    }

    public void signInServer(String email, String password, RepositorySignInListener repositorySignInListener) {
        this.repositorySignInListener = repositorySignInListener;
        LoginDAO.signInServer(email, password, this);
    }

    public void signUpServer(String name, String email, String password, RepositoryRegisterListener repositoryRegisterListener) {
        this.repositoryRegisterListener = repositoryRegisterListener;
        LoginDAO.signUpServer(name, email, password, this);
    }

    static {
        userRepository = new UserRepository();
    }

    public UserRepository() {
        userRoomDAO = EasyFlatDatabase.getDatabase().userRoomDAO();
    }

    public void addAll(List<User> users) {
        try {
            EasyFlatDatabase.databaseWriteExecutor.submit(() -> userRoomDAO.addUsers(users)).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        EasyFlatDatabase.databaseWriteExecutor.submit(() -> userRoomDAO.deleteAll());
    }

    public User getUser(int user_id) {
        User user = null;
        try {
            user = EasyFlatDatabase.databaseWriteExecutor.submit(() -> userRoomDAO.getUser(user_id)).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        try {
            users = EasyFlatDatabase.databaseWriteExecutor.submit(() -> userRoomDAO.getAll()).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return users;
    }

    public long add(User user) {
        long rowId = -1;
        try {
            rowId = EasyFlatDatabase.databaseWriteExecutor.submit(() -> userRoomDAO.addUser(user)).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return rowId;
    }

    public static UserRepository getUserRepository() {
        return userRepository;
    }


    @Override
    public void onSuccessUser(User user) {
        repositoryGetUserListener.onSuccessGetUser(user);
    }

    @Override
    public void onErrorUser(String error) {
        repositoryGetUserListener.onErrorGetUser(error);
    }

    @Override
    public void onSuccessSignIn(String token) {
        repositorySignInListener.onSuccessSignIn(token);
    }

    @Override
    public void onErrorLogin(String error) {
        repositorySignInListener.onErrorLogin(error);
    }

    @Override
    public void onSuccessRegister(String message) {
        repositoryRegisterListener.onSuccessRegister(message);
    }

    @Override
    public void onErrorRegister(String error) {
        repositoryRegisterListener.onErrorRegister(error);
    }
}
