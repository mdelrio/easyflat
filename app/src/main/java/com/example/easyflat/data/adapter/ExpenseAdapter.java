package com.example.easyflat.data.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easyflat.R;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.utils.CommonUtils;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.text.ParseException;
import java.util.ArrayList;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ExpenseHolder> {
    public interface OnManageExpenseListener {
        void onExpenseEdit(Expense expense);
    }

    public interface OnDeleteExpenseListener {
        void onExpenseDelete(Expense expense);
    }

    private Context context;
    private ArrayList<Expense> expenses;
    private OnDeleteExpenseListener listener;

    public ExpenseAdapter(Context context, ArrayList<Expense> expenses, OnDeleteExpenseListener listener) {
        this.context = context;
        this.expenses = expenses;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ExpenseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new ExpenseHolder(layoutInflater.inflate(R.layout.expense_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenseHolder holder, int position) {
        Expense expense = expenses.get(position);
        holder.tvName.setText(expense.getSubject());
        holder.tvQuantity.setText(String.format(CommonUtils.QUANTITY_FORMAT, expense.getPrice()));
        try {
            holder.tvDate.setText(CommonUtils.getDateTimeFormat().format(CommonUtils.getSimpleDateFormat().parse(expense.getCreated_at())));
        } catch (ParseException e) {
            holder.tvDate.setText("");
        }
        if (position == 0) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.itemView.findViewById(R.id.cvExpense).getLayoutParams();
            layoutParams.setMargins(context.getResources().getDimensionPixelOffset(R.dimen.card_margin), context.getResources().getDimensionPixelOffset(R.dimen.card_margin), context.getResources().getDimensionPixelOffset(R.dimen.card_margin), context.getResources().getDimensionPixelOffset(R.dimen.card_margin));
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OnManageExpenseListener) context).onExpenseEdit(expense);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialAlertDialogBuilder(context).setTitle(R.string.delete).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onExpenseDelete(expense);
                    }
                }).setNegativeButton(android.R.string.no, null).show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

    public class ExpenseHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvQuantity;
        TextView tvDate;

        public ExpenseHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvDate = itemView.findViewById(R.id.tvDate);
            itemView.setOnClickListener(v -> ((OnManageExpenseListener) context).onExpenseEdit(expenses.get(getAdapterPosition())));
        }
    }
}

