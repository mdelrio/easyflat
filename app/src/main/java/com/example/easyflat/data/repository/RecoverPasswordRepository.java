package com.example.easyflat.data.repository;

import com.example.easyflat.data.dao.connection.LoginDAO;

public class RecoverPasswordRepository {
    private static RecoverPasswordRepository recoverPasswordRepository;

    static {
        recoverPasswordRepository = new RecoverPasswordRepository();
    }

    public static RecoverPasswordRepository getRepository() {
        return recoverPasswordRepository;
    }

    public void sendRecoverPasswordMail(String email, LoginDAO.ResponseRecoverPasswordApi responseRecoverPasswordApi) {
        LoginDAO.recoverPassword(email, responseRecoverPasswordApi);
    }
}
