package com.example.easyflat.data.model.debt;

import java.util.List;

public class DebtResponse {
    private List<Debt> debts;
    private Debt debt;
    private String error;

    public List<Debt> getDebts() {
        return debts;
    }

    public void setDebts(List<Debt> debts) {
        this.debts = debts;
    }

    public Debt getDebt() {
        return debt;
    }

    public void setDebt(Debt debt) {
        this.debt = debt;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
