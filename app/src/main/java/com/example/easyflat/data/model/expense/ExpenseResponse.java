package com.example.easyflat.data.model.expense;

import com.example.easyflat.data.model.group.Group;

import java.util.List;

public class ExpenseResponse {
    private List<Expense> expenses;
    private Expense expense;
    private String error;

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public Expense getExpense() {
        return expense;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
