package com.example.easyflat.data.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.message.Message;

import java.util.List;

@Dao
public interface DebtRoomDAO {

    @Query("SELECT * FROM Debt")
    List<Debt> getDebts();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<Debt> debts);

    @Query("SELECT a.id as id, a.expense_id as expense_id, a.user_id as user_id, b.price as price FROM Debt AS a INNER JOIN (SELECT user_id, SUM(price) as price FROM Debt GROUP BY user_id) as b ON a.user_id = b.user_id GROUP BY a.user_id")
    List<Debt> getUsersTotalPrice();

    @Query("SELECT * FROM Debt where expense_id = :expense_id")
    List<Debt> getDebtsExpense(int expense_id);

    @Query("DELETE FROM Debt")
    void deleteDebts();
}
