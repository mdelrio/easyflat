package com.example.easyflat.data.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.easyflat.data.model.User;

import java.util.List;

import retrofit2.http.DELETE;

@Dao
public interface UserRoomDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long addUser(User user);

    @Delete
    void delete(User user);

    @Update
    void update(User user);

    @Query("Select * from user")
    List<User> getAll();

    @Query("Delete from user")
    void deleteAll();

    @Query("Select * from user where id = :id ")
    User getUser(int id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addUsers(List<User> users);
}
