package com.example.easyflat.data.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.easyflat.data.model.expense.Expense;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class Month {
    private int month;
    private int year;
    private boolean expanded;
    private ArrayList<Expense> expenses;

    public Month(int month, int year) {
        this.month = month;
        this.year = year;
        this.expanded = false;
        this.expenses = new ArrayList<>();
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public ArrayList<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(Collection<Expense> expenses) {
        this.expenses.clear();
        this.expenses.addAll(expenses);
    }

    public void addExpense(Expense expense) {
        this.expenses.add(expense);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Month month1 = (Month) o;
        return month == month1.month &&
                year == month1.year;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(month, year);
    }
}
