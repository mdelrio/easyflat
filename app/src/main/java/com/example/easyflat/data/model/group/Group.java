package com.example.easyflat.data.model.group;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
public class Group {

    @Ignore
    public static final String KEY = "GROUP";
    //region Variables
    @PrimaryKey
    @NonNull
    private int id;

    @NonNull
    private String name;

    @NonNull
    private String code;
    //endregion

    //region Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setCode(@NonNull String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
    //endregion


    public Group(@NonNull String name, @NonNull String code) {
        this.name = name;
        this.code = code;
    }
}
