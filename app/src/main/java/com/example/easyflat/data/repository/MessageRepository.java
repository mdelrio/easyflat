package com.example.easyflat.data.repository;

import com.example.easyflat.data.dao.connection.MessageDAO;
import com.example.easyflat.data.dao.room.ChatRoomDAO;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.model.message.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MessageRepository {
    private static MessageRepository messageRepository;
    private ChatRoomDAO chatRoomDAO;

    static {
        messageRepository = new MessageRepository();
    }

    private MessageRepository() {
        chatRoomDAO = EasyFlatDatabase.getDatabase().chatRoomDAO();
    }

    public static MessageRepository getRepository() {
        return messageRepository;
    }

    public void sendMessage(String message, int group_id, int user_id, MessageDAO.GetMessagesDAOListener getMessagesDAOListener){
        MessageDAO.sendMessage(message, group_id, user_id, getMessagesDAOListener);
    }

    public void getAll(int group_id, MessageDAO.GetMessagesDAOListener getMessagesDAOListener) {
        MessageDAO.getMessages(group_id, getMessagesDAOListener);
    }

    public void addAll(List<Message> messages){
        EasyFlatDatabase.databaseWriteExecutor.submit(()->chatRoomDAO.insertAll(messages));
    }

    public long addMessage(final Message message) {
        long rowId = -1;
        try {
            rowId = EasyFlatDatabase.databaseWriteExecutor.submit(() -> chatRoomDAO.addMessage(message)).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return rowId;
    }
}
