package com.example.easyflat.data.model.group;

import java.util.List;

public class GroupResponse {
    private List<Group> groups;
    private Group group;
    private int id;
    private String message;
    private String error;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> group) {
        this.groups = group;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
