package com.example.easyflat.data.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easyflat.R;
import com.example.easyflat.data.model.Month;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.utils.ValueFormatter;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MonthAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private ArrayList<Month> months;
    private ArrayList<Debt> debts;
    private ExpenseAdapter.OnDeleteExpenseListener listener;
    private boolean visible;

    public MonthAdapter(Context context, ExpenseAdapter.OnDeleteExpenseListener listener) {
        this.context = context;
        months = new ArrayList<>();
        debts = new ArrayList<>();
        this.listener = listener;
        this.visible = false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (viewType != 0) {
            return new MonthHolder(layoutInflater.inflate(R.layout.expense_month_item, parent, false));
        } else {
            return new GroupHolder(layoutInflater.inflate(R.layout.group_debts_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (position == 0 && visible) {
            ArrayList<PieEntry> debtEntries = new ArrayList<>();
            ArrayList<PieEntry> settleEntries = new ArrayList<>();
            for (Debt debt : debts) {
                PieEntry entry;
                if (debt.getPrice() < 0f) {
                    entry = new PieEntry(debt.getPrice() * -1f, debt.getName());
                    debtEntries.add(entry);
                } else {
                    entry = new PieEntry(debt.getPrice(), debt.getName());
                    settleEntries.add(entry);
                }
            }
            PieDataSet debtDataSet = new PieDataSet(debtEntries, context.getString(R.string.debtor));
            PieDataSet settleDataSet = new PieDataSet(settleEntries, context.getString(R.string.settled));
            debtDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
            settleDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
            debtDataSet.setSliceSpace(2f);
            settleDataSet.setSliceSpace(2f);
            debtDataSet.setValueTextColor(Color.BLACK);
            settleDataSet.setValueTextColor(Color.BLACK);
            debtDataSet.setValueTextSize(12f);
            settleDataSet.setValueTextSize(12f);
            PieData debtData = new PieData(debtDataSet);
            PieData settleData = new PieData(settleDataSet);
            debtData.setValueFormatter(new ValueFormatter());
            settleData.setValueFormatter(new ValueFormatter());
            ((GroupHolder) holder).pcDebtor.setData(debtData);
            ((GroupHolder) holder).pcSettled.setData(settleData);

        } else {
            Month month = months.get(position - (visible ? 1 : 0));
            switch (month.getMonth()) {
                case 1:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.january));
                    break;
                case 2:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.february));
                    break;
                case 3:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.march));
                    break;
                case 4:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.april));
                    break;
                case 5:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.may));
                    break;
                case 6:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.juny));
                    break;
                case 7:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.july));
                    break;
                case 8:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.august));
                    break;
                case 9:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.septembre));
                    break;
                case 10:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.octobre));
                    break;
                case 11:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.novembre));
                    break;
                case 12:
                    ((MonthHolder) holder).tvMonth.setText(context.getString(R.string.decembre));
                    break;
                default:
                    ((MonthHolder) holder).tvMonth.setText("Month");
            }
            ((MonthHolder) holder).tvYear.setText(String.valueOf(month.getYear()));
            ((MonthHolder) holder).tvExpensesNumber.setText(String.valueOf(month.getExpenses().size()));
            ExpenseAdapter adapter = new ExpenseAdapter(context, month.getExpenses(), listener);
            ((MonthHolder) holder).rvExpenses.setAdapter(adapter);
            ((MonthHolder) holder).rvExpenses.setVisibility(month.isExpanded() ? View.VISIBLE : View.GONE);
            if (month.getExpenses().size() > 0) {
                ((MonthHolder) holder).itemView.setOnClickListener(v -> {
                    month.setExpanded(!month.isExpanded());
                    notifyItemChanged(position);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        if (visible) {
            return months.size() + 1;
        } else {
            return months.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && visible) {
            return 0;
        } else {
            return 1;
        }
    }

    public void addAll(Collection<Month> months) {
        this.months.clear();
        this.months.addAll(months);
        if (this.months.get(0).getExpenses().size() > 0) {
            this.months.get(0).setExpanded(true);
        }
        notifyDataSetChanged();
    }

    public void addDebts(List<Debt> debts) {
        this.debts.clear();
        this.debts.addAll(debts);
        visible = false;
        for (Debt debt :
                debts) {
            if (debt.getPrice() != 0f) {
                visible = true;
                break;
            }
        }
        notifyDataSetChanged();
    }

    public class MonthHolder extends RecyclerView.ViewHolder {
        TextView tvMonth;
        TextView tvYear;
        TextView tvExpensesNumber;
        RecyclerView rvExpenses;

        public MonthHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tvMonth);
            tvYear = itemView.findViewById(R.id.tvYear);
            tvExpensesNumber = itemView.findViewById(R.id.tvExpensesNumber);
            rvExpenses = itemView.findViewById(R.id.rvExpenses);
            rvExpenses.setLayoutManager(new LinearLayoutManager(context));
        }
    }

    public class GroupHolder extends RecyclerView.ViewHolder {
        PieChart pcDebtor;
        PieChart pcSettled;

        public GroupHolder(@NonNull View itemView) {
            super(itemView);
            pcDebtor = itemView.findViewById(R.id.pcDebtor);
            pcSettled = itemView.findViewById(R.id.pcSettled);
            pcDebtor.setCenterText(context.getString(R.string.debtor));
            pcSettled.setCenterText(context.getString(R.string.settled));
            pcDebtor.setCenterTextSize(10f);
            pcSettled.setCenterTextSize(10f);
            pcDebtor.setHoleRadius(45f);
            pcSettled.setHoleRadius(45f);
            pcDebtor.setTransparentCircleRadius(60f);
            pcSettled.setTransparentCircleRadius(60f);
            pcDebtor.getDescription().setEnabled(false);
            pcSettled.getDescription().setEnabled(false);
            pcDebtor.setEntryLabelColor(Color.BLACK);
            pcSettled.setEntryLabelColor(Color.BLACK);
            pcDebtor.getLegend().setEnabled(false);
            pcSettled.getLegend().setEnabled(false);
            pcDebtor.setFocusable(false);
            pcSettled.setFocusable(false);
        }
    }
}

