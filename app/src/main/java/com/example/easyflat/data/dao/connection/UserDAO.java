package com.example.easyflat.data.dao.connection;

import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.UsersResponse.UsersResponse;
import com.example.easyflat.data.network.ApiTokenRestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDAO {

    public interface ResponseListUsersApi{
        void onSuccessObtained(List<User> users);
        void onError(String error);
    }

    public static void getUsers(final int group_id, final ResponseListUsersApi responseListUsersApi) {
        Call<UsersResponse> call = ApiTokenRestClient.getInstance().getUsers(group_id);
        call.enqueue(new Callback<UsersResponse>() {

            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {
                if (response.isSuccessful()) {
                    responseListUsersApi.onSuccessObtained(response.body().getUsers());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        responseListUsersApi.onError(errorObject.getString("error"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        responseListUsersApi.onError(e.getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {
                responseListUsersApi.onError(t.getMessage());
            }
        });
    }
}
