package com.example.easyflat.data.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.model.message.Message;

import java.util.List;

@Dao
public interface ExpenseRoomDAO {
    @Query("SELECT * FROM Expense")
    List<Expense> getExpenses();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addExpense(Expense expense);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<Expense> expenses);

    @Query("DELETE FROM expense WHERE id = :id")
    void delete(int id);
}
