package com.example.easyflat.data.model.debt;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.expense.Expense;

import java.util.Objects;

@Entity(foreignKeys = {@ForeignKey(entity = Expense.class,
        parentColumns = "id",
        childColumns = "expense_id"),
        @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id")}, indices = {@Index("expense_id"), @Index("user_id")})
public class Debt {

    @PrimaryKey
    @NonNull
    private int id;
    @NonNull
    private int user_id;
    @Ignore
    private String name;
    @NonNull
    private int expense_id;
    @NonNull
    private float price;
    @Ignore
    private boolean isChecked;

    public Debt(int user_id, int expense_id, float price) {
        this.user_id = user_id;
        this.expense_id = expense_id;
        this.price = price;
        this.isChecked = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(int expense_id) {
        this.expense_id = expense_id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Debt debt = (Debt) o;
        return user_id == debt.user_id;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(user_id);
    }
}
