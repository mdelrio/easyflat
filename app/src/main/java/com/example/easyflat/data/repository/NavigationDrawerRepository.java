package com.example.easyflat.data.repository;

import com.example.easyflat.data.dao.room.EasyFlatDatabase;

public class NavigationDrawerRepository {

    private static NavigationDrawerRepository navigationDrawerRepository;

    static {
        navigationDrawerRepository = new NavigationDrawerRepository();
    }

    public static NavigationDrawerRepository getNavigationDrawerRepository() {
        return navigationDrawerRepository;
    }

    public void clearAllDatabase(){
        EasyFlatDatabase.databaseWriteExecutor.execute(() -> EasyFlatDatabase.getDatabase().clearAllTables());
    }
}
