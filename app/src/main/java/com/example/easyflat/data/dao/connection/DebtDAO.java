package com.example.easyflat.data.dao.connection;

import androidx.room.Dao;

import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.debt.DebtResponse;
import com.example.easyflat.data.model.expense.ExpenseResponse;
import com.example.easyflat.data.network.ApiTokenRestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Dao
public class DebtDAO {

    public interface GetDebtsListener{
        void onSuccessGetDebts(List<Debt> debts);
        void onErrorGetDebts(String error);
    }

    public static void getDebts(final int group_id, final GetDebtsListener getDebtsListener) {
        Call<DebtResponse> call = ApiTokenRestClient.getInstance().getDebts(group_id);
        call.enqueue(new Callback<DebtResponse>() {

            @Override
            public void onResponse(Call<DebtResponse> call, Response<DebtResponse> response) {
                if (response.isSuccessful()) {
                    getDebtsListener.onSuccessGetDebts(response.body().getDebts());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            getDebtsListener.onErrorGetDebts(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<DebtResponse> call, Throwable t) {
                getDebtsListener.onErrorGetDebts(t.getMessage());
            }
        });
    }
}
