package com.example.easyflat.data.model.login;

public class RecoverPasswordResponse {
    private String message;

    private String error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String success) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
