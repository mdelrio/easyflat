package com.example.easyflat.data.network;

import com.blogspot.atifsoftwares.animatoolib.BuildConfig;
//import com.example.easyflat.BuildConfig;
import com.example.easyflat.R;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiTokenRestClient {
    private static ApiTokenService API_SERVICE;

    private static final String BASE_URL = "https://debug.easyflat.ovh/";

    public static synchronized ApiTokenService getInstance() {

        if (API_SERVICE == null) {

            OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(5, TimeUnit.SECONDS);

            okHttpBuilder.addInterceptor(new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BASIC)
                    .addHeader("Accept", "application/json")
                    .addHeader("authorization", "Bearer " + CommonUtils.readSharedPreference(EasyFlatApplication.getAppContext().getString(R.string.shared_preferences_login), CommonUtils.KEY_TOKEN))
                    .log(Platform.INFO).build());

            Gson gson = new GsonBuilder()
                    .setDateFormat("dd-MM-yyyy")
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpBuilder.build())
                    .build();

            API_SERVICE = retrofit.create(ApiTokenService.class);
        }
        return API_SERVICE;
    }
}
