package com.example.easyflat.data.network;

import com.example.easyflat.data.model.login.LoginResponse;
import com.example.easyflat.data.model.login.LogoutResponse;
import com.example.easyflat.data.model.login.RecoverPasswordResponse;
import com.example.easyflat.data.model.login.RegisterResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiService {

    @FormUrlEncoded
    @POST("api/register")
    Call<RegisterResponse> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password);


    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponse> login(
            @Field("email") String email,
            @Field("password") String password);

    @POST("api/logout")
    Call<LogoutResponse> logout(
            @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api/create")
    Call<RecoverPasswordResponse> recoverPassword(
            @Field("email") String email);
}

