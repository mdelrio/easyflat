package com.example.easyflat.data.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.easyflat.R;
import com.example.easyflat.ui.EasyFlatApplication;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class CommonUtils {

    public final static String KEY_TOKEN = "Token";
    public static final String QUANTITY_FORMAT = "%.2f";

    public static String REGEX_EMAIL = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static String REGEX_PASSWORD = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";

    public static SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public static SimpleDateFormat getDateTimeFormat() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm");
    }

    public static String getDateAndHourFormat(Date date) {
        DateFormat hourAndDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return hourAndDateFormat.format(date);
    }

    public static void showToast(String message) {
        Toast.makeText(EasyFlatApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
    }

    public static void saveSharedPreference(String file, String key, String value) {
        SharedPreferences sharedPreferences = EasyFlatApplication.getAppContext().getSharedPreferences(file, Context.MODE_PRIVATE);
        SharedPreferences.Editor sEditor = sharedPreferences.edit();
        sEditor.putString(key, value);
        sEditor.apply();
    }

    public static void saveSharedPreference(String file, String key, int value) {
        SharedPreferences sharedPreferences = EasyFlatApplication.getAppContext().getSharedPreferences(file, Context.MODE_PRIVATE);
        SharedPreferences.Editor sEditor = sharedPreferences.edit();
        sEditor.putInt(key, value);
        sEditor.apply();
    }

    public static String readSharedPreference(String file, String key) {
        SharedPreferences sharedPreferences = EasyFlatApplication.getAppContext().getSharedPreferences(file, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public static int readIntSharedPreference(String file, String key) {
        SharedPreferences sharedPreferences = EasyFlatApplication.getAppContext().getSharedPreferences(file, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, -1);
    }

    public static void clearSharedPreferences(String file) {
        SharedPreferences sharedPreferences = EasyFlatApplication.getAppContext().getSharedPreferences(file, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public static String listToString(List<Integer> list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i < list.size() - 1)
                stringBuilder.append(list.get(i) + ",");
            else
                stringBuilder.append(list.get(i));
        }
        return stringBuilder.toString();
    }
}
