package com.example.easyflat.data.network;

import com.example.easyflat.data.model.UsersResponse.UsersResponse;
import com.example.easyflat.data.model.debt.DebtResponse;
import com.example.easyflat.data.model.expense.ExpenseResponse;
import com.example.easyflat.data.model.group.GroupResponse;
import com.example.easyflat.data.model.login.ProfileResponse;
import com.example.easyflat.data.model.message.Message;
import com.example.easyflat.data.model.message.MessageResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiTokenService {

    @GET("api/user")
    Call<ProfileResponse> getUser();

    @GET("api/groups")
    Call<GroupResponse> getGroups();

    @FormUrlEncoded
    @POST("api/groups")
    Call<GroupResponse> createGroup(
            @Field("name") String name);

    @PUT("api/groups/{code}")
    Call<GroupResponse> joinGroup(
            @Path("code") String code);

    @FormUrlEncoded
    @POST("api/users")
    Call<UsersResponse> getUsers(
            @Field("group_id") int group_id
    );

    @GET("api/messages/{group_id}")
    Call<MessageResponse> getMessages(@Path("group_id") int id);

    @GET("api/message")
    Call<MessageResponse> getMessage();

    @FormUrlEncoded
    @POST("api/messages")
    Call<MessageResponse> sendMessage(
            @Field("message") String message,
            @Field("group_id") int group_id,
            @Field("user_id") int user_id);

    @GET("api/expenses/{group_id}")
    Call<ExpenseResponse> getExpenses(
            @Path("group_id") int group_id);

    @FormUrlEncoded
    @POST("api/expenses")
    Call<ExpenseResponse> addExpense(
            @Field("users_id")String users,
            @Field("group_id") int group_id,
            @Field("subject") String subject,
            @Field("description") String description,
            @Field("price") float price);

    @FormUrlEncoded
    @POST("api/editexpense")
    Call<ExpenseResponse> editExpense(
            @Field("id") int id,
            @Field("users_id") String users_id,
            @Field("subject") String subject,
            @Field("description") String description,
            @Field("price") float price);

    @DELETE("api/expenses/{id}")
    Call<ExpenseResponse> deleteExpense(
            @Path("id") int id);


    @DELETE("api/groups/{id}")
    Call<GroupResponse> exitGroup(
            @Path("id") int id);

    @GET("api/debts/{group_id}")
    Call<DebtResponse> getDebts(
            @Path("group_id") int group_id);
}
