package com.example.easyflat.data.dao.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.easyflat.data.model.message.Message;

import java.util.List;

@Dao
public interface ChatRoomDAO {
        @Query("SELECT * FROM Message")
        List<Message> getMessages();

        @Insert
        long addMessage(Message message);

        @Insert(onConflict = OnConflictStrategy.IGNORE)
        void insertAll(List<Message> messages);
}
