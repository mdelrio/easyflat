package com.example.easyflat.data.dao.connection;

import com.example.easyflat.data.model.message.Message;
import com.example.easyflat.data.model.message.MessageResponse;
import com.example.easyflat.data.network.ApiTokenRestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageDAO {

    public interface GetMessagesDAOListener {
        void onSuccesMessagesGet(List<Message> messages);
        void onErrorMessagesGet(String error);
        void onSuccesMessageGet(Message message);
        void onErrorMessageGet(String error);
        void onSuccessSendMessage(Message message);
        void onErrorSendMessage(String message);
    }

    public static void getMessages(final int group_id, final GetMessagesDAOListener getMessagesDAOListener) {
        Call<MessageResponse> call = ApiTokenRestClient.getInstance().getMessages(group_id);
        call.enqueue(new Callback<MessageResponse>() {

            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response.isSuccessful()) {
                    getMessagesDAOListener.onSuccesMessagesGet(response.body().getMessages());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            getMessagesDAOListener.onErrorMessagesGet(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                getMessagesDAOListener.onErrorMessagesGet(t.getMessage());
            }
        });
    }

    public static void getMessage(final GetMessagesDAOListener getMessagesDAOListener) {
        Call<MessageResponse> call = ApiTokenRestClient.getInstance().getMessage();
        call.enqueue(new Callback<MessageResponse>() {

            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response.isSuccessful()) {
                    getMessagesDAOListener.onSuccesMessageGet(response.body().getMessage());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            getMessagesDAOListener.onErrorMessageGet(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                getMessagesDAOListener.onErrorMessageGet(t.getMessage());
            }
        });
    }

    public static void sendMessage(final String message, final int group_id, final int user_id, final GetMessagesDAOListener getMessagesDAOListener) {
        Call<MessageResponse> call = ApiTokenRestClient.getInstance().sendMessage(message, group_id, user_id);
        call.enqueue(new Callback<MessageResponse>() {

            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response.isSuccessful()) {
                    getMessagesDAOListener.onSuccessSendMessage(response.body().getMessage());
                } else {
                    JSONObject errorObject = null;
                    try {
                        errorObject = new JSONObject(response.errorBody().string());
                        if (errorObject.getJSONObject("message").getString("error") != null)
                            getMessagesDAOListener.onErrorSendMessage(errorObject.getJSONObject("message").getString("error"));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                getMessagesDAOListener.onErrorSendMessage(t.getMessage());
            }
        });
    }
}
