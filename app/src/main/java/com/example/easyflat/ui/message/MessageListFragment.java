package com.example.easyflat.ui.message;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easyflat.R;
import com.example.easyflat.data.adapter.MessageAdapter;
import com.example.easyflat.data.model.message.Message;
import com.example.easyflat.data.utils.CommonUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MessageListFragment extends Fragment implements MessageListContract.View{

    public static final String TAG = "MessageListFragment";

    private RecyclerView recyclerView;
    private MessageAdapter adapter;
    private MessageListContract.Presenter presenter;
    private ProgressBar progressBar;
    private FloatingActionButton btSend;
    private EditText edText;

    public static Fragment newInstance() {
        return new MessageListFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_message_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.pbMessage);
        recyclerView = view.findViewById(R.id.rvMessage);
        edText = view.findViewById(R.id.edMessageText);
        btSend = view.findViewById(R.id.btSendMessage);
        initRecycler();

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sendMessage(edText.getText().toString());
            }
        });
    }

    private void initRecycler() {
        adapter = new MessageAdapter(getContext());
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
//        linearLayoutManager.setReverseLayout(true);
//        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        presenter.load();
    }

    @Override
    public void showListMessages(List<Message> messages) {
        adapter.clear();
        adapter.addAll(messages);
        recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessAdd(Message message) {
        edText.setText(null);
        adapter.add(message);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorAdd() {

    }

    @Override
    public void setPresenter(MessageListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(int idError) {
        CommonUtils.showToast(getString(idError));
    }

    @Override
    public void listEmpty() {
        CommonUtils.showToast(getString(R.string.message_list_empty));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
