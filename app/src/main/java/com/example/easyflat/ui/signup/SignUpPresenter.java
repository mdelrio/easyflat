package com.example.easyflat.ui.signup;

import com.example.easyflat.data.repository.UserRepository;
import com.example.easyflat.data.utils.CommonUtils;

public class SignUpPresenter implements SignUpContract.Presenter, UserRepository.RepositoryRegisterListener {

    private SignUpContract.View view;

    SignUpPresenter(SignUpContract.View view) {
        this.view = view;
    }

    @Override
    public void add(String name, String email, String password) {
        UserRepository.getUserRepository().signUpServer(name, email, password, this);
    }

    public void onSuccessRegister(String message) {
        view.onSuccess();
        CommonUtils.showToast(message);
    }

    public void onErrorRegister(String error) {
        view.userExists();
    }
}
