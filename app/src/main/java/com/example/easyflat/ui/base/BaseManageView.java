package com.example.easyflat.ui.base;

public interface BaseManageView<T> {
    void onSuccess();
    void showError(int idError);
    void setPresenter(T presenter);
}
