package com.example.easyflat.ui.splash;

import com.example.easyflat.R;
import com.example.easyflat.data.dao.connection.GroupDAO;
import com.example.easyflat.data.dao.connection.UserDAO;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.repository.GroupRepository;
import com.example.easyflat.data.repository.UserRepository;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.List;

public class SplashPresenter implements SplashContract.Presenter, UserRepository.RepositoryGetUserListener, GroupDAO.GetGroupsDAOListener, UserDAO.ResponseListUsersApi {

    private SplashContract.View view;

    public SplashPresenter(SplashContract.View view) {
        this.view = view;
    }

    @Override
    public void checkPreferences() {
        String token = CommonUtils.readSharedPreference(EasyFlatApplication.getAppContext().getString(R.string.shared_preferences_login), CommonUtils.KEY_TOKEN);
        if (!token.equals(""))
            UserRepository.getUserRepository().getUser(this);
        else
            view.notUserExists();
    }

    @Override
    public void checkGroups() {
        GroupRepository.getGroupRepository().downloadGroups(this);
    }

    @Override
    public void onSuccessGetUser(User user) {
        view.onUserExists();
        EasyFlatApplication.setUser(user);
        UserRepository.getUserRepository().add(user);
    }

    @Override
    public void onErrorGetUser(String error) {
        view.notUserExists();
    }

    @Override
    public void onSuccessGroupsGet(List<Group> groups) {
        GroupRepository.getGroupRepository().addAll(groups);

        if (GroupRepository.getGroupRepository().getGroups().size() != 0) {
            int groupid = CommonUtils.readIntSharedPreference(EasyFlatApplication.getAppContext().getString(R.string.shared_preferences_group), Group.KEY);
            if (groupid == -1) {
//                CommonUtils.saveSharedPreference(EasyFlatApplication.getAppContext().getString(R.string.shared_preferences_login), Group.KEY, groups.get(0).getId());
                EasyFlatApplication.setGroup(GroupRepository.getGroupRepository().getGroups().get(0));
                UserRepository.getUserRepository().downloadUsers(EasyFlatApplication.getGroup().getId(), this);
            } else {
                EasyFlatApplication.setGroup(GroupRepository.getGroupRepository().getGroup(groupid));
            }
        }
        else
            view.notGroupsExists();
    }

    @Override
    public void onErrorGroupsGet(String error) {

    }

    @Override
    public void onSuccessObtained(List<User> users) {
        UserRepository.getUserRepository().addAll(users);
        view.onGroupsExists();
    }

    @Override
    public void onError(String error) {

    }
}
