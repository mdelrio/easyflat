package com.example.easyflat.ui.expense;

import android.os.Bundle;
import android.os.Debug;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easyflat.R;
import com.example.easyflat.data.adapter.ExpenseAdapter;
import com.example.easyflat.data.adapter.MonthAdapter;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.model.Month;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;

public class ExpenseListFragment extends Fragment implements ExpenseListContract.View, ExpenseAdapter.OnDeleteExpenseListener {
    @Override
    public void onExpenseDelete(Expense expense) {
        presenter.delete(expense.getId());
    }

    public interface OnManageExpenseListener {
        void onExpenseAdd();
    }

    public final static String TAG = "ExpenseListFragment";
    private RecyclerView rvExpenses;
    private FloatingActionButton fabAddExpense;
    private ProgressBar pbExpenses;
    private MonthAdapter adapter;
    ExpenseListContract.Presenter presenter;

    public static Fragment newInstance() {
        return new ExpenseListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_expense_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fabAddExpense = view.findViewById(R.id.fabExpense);
        pbExpenses = view.findViewById(R.id.pbExpense);
        rvExpenses = view.findViewById(R.id.rvExpenses);
        initRecycler();
        initFloatingActionButton();
    }

    private void initRecycler() {
        adapter = new MonthAdapter(getActivity(), this);
        rvExpenses.setLayoutManager(new LinearLayoutManager(getContext()));
        rvExpenses.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.load();
    }

    private void initFloatingActionButton() {
        fabAddExpense.setOnClickListener(view -> ((OnManageExpenseListener) getActivity()).onExpenseAdd());
    }

    @Override
    public void showExpenses(ArrayList<Month> months) {
        adapter.addAll(months);
    }

    @Override
    public void showDebts(ArrayList<Debt> debts) {
        adapter.addDebts(debts);
    }

    @Override
    public void showError(String error) {
        CommonUtils.showToast(error);
    }

    @Override
    public void onDeleteSuccess() {
        presenter.load();
    }

    @Override
    public void setPresenter(ExpenseListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showProgressBar() {
        pbExpenses.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbExpenses.setVisibility(View.GONE);
    }

    @Override
    public void showError(int idError) {
        CommonUtils.showToast(getString(idError));
    }

    @Override
    public void listEmpty() {
        CommonUtils.showToast(getString(R.string.message_list_empty));
    }
}

