package com.example.easyflat.ui.group;

public interface GroupContract {
    interface View{
        void onSuccessCreated();
        void onErrorCreated(String message);
        void onSuccessJoined();
        void onErrorJoined(String message);
    }

    interface Presenter{
        void createGroup(String name);
        void joinGroup(String code);
    }
}
