package com.example.easyflat.ui.expense;

import androidx.annotation.Nullable;

import com.example.easyflat.data.dao.connection.DebtDAO;
import com.example.easyflat.data.dao.connection.ExpenseDAO;
import com.example.easyflat.data.dao.connection.UserDAO;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.repository.DebtRepository;
import com.example.easyflat.data.repository.ExpenseRepository;
import com.example.easyflat.data.repository.UserRepository;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.ArrayList;
import java.util.List;

public class ExpenseManagePresenter implements ExpenseManageContract.Presenter, UserDAO.ResponseListUsersApi, ExpenseDAO.ExpensesManageDAOListener, DebtDAO.GetDebtsListener {
    ExpenseManageContract.View view;

    public ExpenseManagePresenter(ExpenseManageContract.View view) {
        this.view = view;
    }

    @Override
    public void add(Expense expense, List<Integer> users) {
        ExpenseRepository.getExpenseRepository().addExpense(CommonUtils.listToString(users), expense.getGroup_id(), expense.getSubject(), expense.getDescription(), expense.getPrice(), this);
    }

    @Override
    public void edit(Expense expense, List<Integer> users) {
        ExpenseRepository.getExpenseRepository().editExpense(expense.getId(), CommonUtils.listToString(users), expense.getSubject(), expense.getDescription(), expense.getPrice(), this);
    }

    @Override
    public void load(int expenseId) {
        if (expenseId != -1) {
            List<Debt> debts = DebtRepository.getDebtRepository().getDebtExpense(expenseId);
            List<User> users = UserRepository.getUserRepository().getUsers();
            for (Debt debt : debts) {
                debt.setName(UserRepository.getUserRepository().getUser(debt.getUser_id()).getName());
            }
            for (User user : users) {
                if (!debts.contains(new Debt(user.getId(), expenseId, 0))) {
                    Debt debt = new Debt(user.getId(), expenseId, 0);
                    debt.setChecked(false);
                    debts.add(debt);
                }
            }
            view.showUsers(debts);
        } else {
            onSuccessObtained(UserRepository.getUserRepository().getUsers());
        }
    }

    @Override
    public void onSuccessObtained(List<User> users) {
        ArrayList<Debt> debts = new ArrayList<>();
        for (User user : users) {
            Debt debt = new Debt(user.getId(), 0, 0f);
            debt.setName(user.getName());
            debts.add(debt);
        }
        view.showUsers(debts);
    }

    @Override
    public void onError(String error) {
    }

    @Override
    public void onSuccessAddExpense(Expense expense) {
       view.onSuccess();
    }

    @Override
    public void onErrorAddExpense(String error) {
    }

    @Override
    public void onSuccessEdit(Expense expense) {
       view.onSuccess();
    }

    @Override
    public void onErrorEdit(String error) {
        view.showError(error);
    }

    @Override
    public void onSuccessGetDebts(List<Debt> debts) {
        DebtRepository.getDebtRepository().addAll(debts);
        view.onSuccess();
    }

    @Override
    public void onErrorGetDebts(String error) {
    }
}

