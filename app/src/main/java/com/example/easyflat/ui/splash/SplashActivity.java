package com.example.easyflat.ui.splash;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.easyflat.R;
import com.example.easyflat.data.model.message.Message;
import com.example.easyflat.ui.navigationdrawer.NavigationDrawerActivity;
import com.example.easyflat.ui.group.GroupActivity;
import com.example.easyflat.ui.login.LoginActivity;

public class SplashActivity extends AppCompatActivity implements SplashContract.View {

    private static final long WAIT_TIME = 2000;
    Intent intent;
    private SplashContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        presenter = new SplashPresenter(this);
        presenter.checkPreferences();
    }

    @Override
    public void onUserExists() {
        presenter.checkGroups();
    }

    @Override
    public void notUserExists() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initLogin();
            }
        }, WAIT_TIME);
    }

    @Override
    public void onGroupsExists() {
        intent = new Intent(this, NavigationDrawerActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void notGroupsExists() {
        intent = new Intent(this, GroupActivity.class);
        startActivity(intent);
        finish();
    }

    private void initLogin() {
        intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }
}
