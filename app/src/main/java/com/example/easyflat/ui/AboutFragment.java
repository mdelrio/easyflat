package com.example.easyflat.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.easyflat.R;
import com.vansuita.materialabout.builder.AboutBuilder;
import com.vansuita.materialabout.views.AboutView;

public class AboutFragment extends Fragment {

    public static final String TAG = "AboutFragment";
    ScrollView scrollView;
    public static Fragment newInstance() {
        return new AboutFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_about,container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        scrollView= view.findViewById(R.id.svAbout);
        AboutView aboutView = AboutBuilder.with(getContext())
                .setPhoto(R.mipmap.profile_picture)
                .setCover(R.mipmap.profile_cover)
                .setName(getString(R.string.about_name))
                .setSubTitle(getString(R.string.about_subTitle))
                .setAppIcon(R.mipmap.ic_launcher)
                .setAppName(R.string.app_name)
                .addGitHubLink(getString(R.string.about_gitHubLink))
                .addLinkedInLink(getString(R.string.about_linkedInLink))
                .addFiveStarsAction()
                .setVersionNameAsAppSubTitle()
                .addEmailLink(getString(R.string.about_EmailLink))
                .addFeedbackAction(getString(R.string.about_feedbackAction))
                .addShareAction(R.string.app_name)
                .setLinksAnimated(true)
                .setShowAsCard(true)
                .build();
        scrollView.addView(aboutView);
    }
}
