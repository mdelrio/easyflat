package com.example.easyflat.ui.base;

public interface BaseListView<T> {
    void setPresenter(T presenter);
    void showProgressBar();
    void hideProgressBar();
    void showError(int idError);
    void listEmpty();
}
