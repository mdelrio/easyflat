package com.example.easyflat.ui.navigationdrawer;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.easyflat.R;
import com.example.easyflat.data.adapter.ExpenseAdapter;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.adapter.GroupAdapter;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.AboutFragment;
import com.example.easyflat.ui.EasyFlatApplication;
import com.example.easyflat.ui.SettingsFragment;
import com.example.easyflat.ui.expense.ExpenseListContract;
import com.example.easyflat.ui.expense.ExpenseListFragment;
import com.example.easyflat.ui.expense.ExpenseListPresenter;
import com.example.easyflat.ui.expense.ExpenseManageContract;
import com.example.easyflat.ui.expense.ExpenseManageFragment;
import com.example.easyflat.ui.expense.ExpenseManagePresenter;
import com.example.easyflat.ui.group.GroupActivity;
import com.example.easyflat.ui.login.LoginActivity;
import com.example.easyflat.ui.message.MessageListContract;
import com.example.easyflat.ui.message.MessageListFragment;
import com.example.easyflat.ui.message.MessageListPresenter;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;

import java.util.List;

public class NavigationDrawerActivity extends AppCompatActivity implements NavigationDrawerContract.View, NavigationView.OnNavigationItemSelectedListener, ExpenseListFragment.OnManageExpenseListener, ExpenseAdapter.OnManageExpenseListener {

    boolean backPressedOnce = false;

    private NavigationView navigationView;
    private Toolbar toolbar;
    private DrawerLayout navigationDrawer;
    private Fragment fragment;
    private String tag;
    private Intent intent;
    private TextView tvActualGroup;
    private ImageView ivAddGroup;
    private RecyclerView rvGroup;
    private ImageButton ibGroupDropdown;
    private GroupAdapter adapter;

    private GroupAdapter.OnclickItemListener onclickItemListener;

    private NavigationDrawerPresenter navigationDrawerPresenter;

    private MessageListContract.Presenter messageListPresenter;

    private Fragment taskManageFragment;

    private ExpenseListContract.Presenter expenseListPresenter;
    private ExpenseManageFragment expenseManageFragment;
    private ExpenseManageContract.Presenter expenseManagePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.nvNavigationView);
        navigationDrawer = findViewById(R.id.nvdLayout);
        toolbar = findViewById(R.id.toolbar);
        setToolbar();
        navigationView.setNavigationItemSelectedListener(this);

        tvActualGroup = navigationView.getHeaderView(0).findViewById(R.id.tvAcualGroup);
        rvGroup = navigationView.getHeaderView(0).findViewById(R.id.rvGroup);
        ibGroupDropdown = navigationView.getHeaderView(0).findViewById(R.id.ibDropdown);
        ivAddGroup = navigationView.getHeaderView(0).findViewById(R.id.iVAddGroup);

        navigationDrawerPresenter = new NavigationDrawerPresenter(this);
        initOnClickItem();
        adapter = new GroupAdapter(onclickItemListener);
        rvGroup.setAdapter(adapter);
        rvGroup.setLayoutManager(new LinearLayoutManager(this));

        tvActualGroup.setText(EasyFlatApplication.getGroup().getName());

        ibGroupDropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rvGroup.getVisibility() == View.GONE) {
                    rvGroup.setVisibility(View.VISIBLE);
                    ivAddGroup.setVisibility(View.VISIBLE);
                }
                else {
                    rvGroup.setVisibility(View.GONE);
                    ivAddGroup.setVisibility(View.GONE);
                }
            }
        });

        navigationDrawerPresenter.loadGroups();

        ivAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavigationDrawerActivity.this, GroupActivity.class);
                startActivity(intent);
            }
        });

        fragment = MessageListFragment.newInstance();
        messageListPresenter = new MessageListPresenter((MessageListContract.View) fragment);
        ((MessageListContract.View) fragment).setPresenter(messageListPresenter);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, fragment, MessageListFragment.TAG);
        fragmentTransaction.commit();
    }

    private void initOnClickItem() {
        onclickItemListener = new GroupAdapter.OnclickItemListener() {
            @Override
            public void change(Group group) {
                navigationDrawerPresenter.changeGroup(group);
            }

            @Override
            public void getInfo(Group group) {
                showGroupInfoDialog(group);
            }
        };
    }
    @Override
    public void showGroups(List<Group> groups) {
        adapter.clear();
        adapter.addAll(groups);
        adapter.notifyDataSetChanged();
    }

    private void showGroupInfoDialog(Group group) {
        new MaterialAlertDialogBuilder(NavigationDrawerActivity.this)
                .setTitle(group.getName())
        .setMessage(R.string.group_delete_message)
        .setNeutralButton(R.string.copy_code, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                copyToClipboard(group);
            }
        })
        .setNegativeButton(R.string.group_out, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                navigationDrawerPresenter.exitGroup(group);
            }
        })
        .show();

    }

    private void copyToClipboard(Group group) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", group.getCode());
        clipboard.setPrimaryClip(clip);
        CommonUtils.showToast(getString(R.string.copied_to_clipboard));
    }

    @Override
    public void onSuccessGroupChange() {
        finish();
        intent = new Intent(NavigationDrawerActivity.this, NavigationDrawerActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSuccessExit() {
        navigationDrawerPresenter.checkGroups();
    }

    @Override
    public void onGroupExists() {
        intent = new Intent(NavigationDrawerActivity.this, NavigationDrawerActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    public void onNotGroupExists() {
        intent = new Intent(NavigationDrawerActivity.this, GroupActivity.class);
        finish();
        startActivity(intent);
    }

    @Override

    public void onSuccessClearDatabase() {

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, navigationDrawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        navigationDrawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //  Se valida que no esté seleccionado el item para no volver a cargar la vista ya cargada
        if (!item.isChecked()) {
//                    Se desmarca el item para marcar el nuevo
            navigationView.getCheckedItem().setChecked(false);
            navigationView.setCheckedItem(item);

            switch (item.getItemId()) {
                case R.id.itNvExpense:
                    fragment = ExpenseListFragment.newInstance();
                    tag = ExpenseListFragment.TAG;
                    expenseListPresenter = new ExpenseListPresenter((ExpenseListFragment) fragment);
                    ((ExpenseListContract.View) fragment).setPresenter(expenseListPresenter);
                    break;

                case R.id.itNvChat:
                    fragment = MessageListFragment.newInstance();
                    tag = MessageListFragment.TAG;
                    messageListPresenter = new MessageListPresenter((MessageListContract.View) fragment);
                    ((MessageListContract.View) fragment).setPresenter(messageListPresenter);
                    break;

                case R.id.itNvAbout:
                    fragment = AboutFragment.newInstance();
                    tag = AboutFragment.TAG;
                    break;

                case R.id.itNvLogout:
                    CommonUtils.clearSharedPreferences(getString(R.string.shared_preferences_login));
                    intent = new Intent(this, LoginActivity.class);
                    finish();
                    startActivity(intent);
                    Animatoo.animateZoom(this);
                    navigationDrawerPresenter.clearDatabase();
                    break;
            }

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, fragment, tag);
            fragmentTransaction.commit();

        }
        navigationDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
//        Se controla que el navigationview esté en la vista o no, en caso de que esté, éste se cierra
        if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
            navigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0 | backPressedOnce) {
                super.onBackPressed();
                return;
            }
            checkDoubleClick();
        }
    }

    //
    private void checkDoubleClick() {
        backPressedOnce = true;
        CommonUtils.showToast(getString(R.string.exit_message));

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                backPressedOnce = false;
            }
        }, 1000);
    }

    @Override
    public void onExpenseAdd() {
        fragment = ExpenseManageFragment.newInstance(null);
        expenseManagePresenter = new ExpenseManagePresenter((ExpenseManageContract.View) fragment);
        ((ExpenseManageContract.View) fragment).setPresenter(expenseManagePresenter);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment, ExpenseManageFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onExpenseEdit(Expense expense) {
        fragment = ExpenseManageFragment.newInstance(expense);
        expenseManagePresenter = new ExpenseManagePresenter((ExpenseManageContract.View) fragment);
        ((ExpenseManageContract.View) fragment).setPresenter(expenseManagePresenter);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment, ExpenseManageFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
