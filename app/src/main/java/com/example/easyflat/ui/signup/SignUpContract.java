package com.example.easyflat.ui.signup;


public interface SignUpContract {
    interface View{
        void onSuccess();
        void userExists();
    }
    interface Presenter{
        void add(String name, String email, String password);
    }
}
