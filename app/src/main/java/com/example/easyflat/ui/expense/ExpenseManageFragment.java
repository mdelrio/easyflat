

package com.example.easyflat.ui.expense;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.easyflat.R;
import com.example.easyflat.data.adapter.PayForAdapter;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.data.utils.DecimalDigitsInputFilter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.  * Use the {@link ExpenseManageFragment#newInstance} factory method to  * create an instance of this fragment.
 */
public class ExpenseManageFragment extends Fragment implements ExpenseManageContract.View {
    public static final String TAG = "expenseManageFragment";
    private static final String ARG_EXPENSE = "expense";
    private Expense expense;
    private RecyclerView rvPayFor;
    private PayForAdapter payForAdapter;
    private FloatingActionButton fabExpenseAdd;
    private TextInputLayout tilSubject;
    private TextInputLayout tilDescription;
    private TextInputLayout tilQuantity;
    private TextInputEditText tiedSubject;
    private TextInputEditText tiedDescription;
    private TextInputEditText tiedQuantity;
    private ExpenseManageContract.Presenter presenter;

    public ExpenseManageFragment() {
        // Required empty public constructor
    }

    public static ExpenseManageFragment newInstance(@Nullable Expense expense) {
        ExpenseManageFragment fragment = new ExpenseManageFragment();
        Bundle args = new Bundle();
        if (expense != null) {
            args.putParcelable(ARG_EXPENSE, expense);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            expense = getArguments().getParcelable(ARG_EXPENSE);
        }
        if (expense == null) {
            expense = new Expense("", "", 0, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_expense_manage, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvPayFor = view.findViewById(R.id.rvPayFor);
        fabExpenseAdd = view.findViewById(R.id.fabExpenseAdd);
        tilSubject = view.findViewById(R.id.tilSubject);
        tilQuantity = view.findViewById(R.id.tilQuantity);
        tilDescription = view.findViewById(R.id.tilDescription);
        tiedSubject = view.findViewById(R.id.tiedSubject);
        tiedDescription = view.findViewById(R.id.tiedDescription);
        tiedQuantity = view.findViewById(R.id.tiedQuantity);
        payForAdapter = new PayForAdapter(getContext());
        rvPayFor.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rvPayFor.setAdapter(payForAdapter);
        presenter.load(expense.getId());
        if (expense.getId() != -1) {
            tiedSubject.setText(expense.getSubject());
            tiedDescription.setText(expense.getDescription());
            tiedQuantity.setText(String.format(CommonUtils.QUANTITY_FORMAT, expense.getPrice()).replace(',', '.'));
            payForAdapter.setQuantity(expense.getPrice());
        }
        fabExpenseAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Integer> users = new ArrayList<>();
                for (Debt debt : payForAdapter.getDebts()) {
                    if (debt.isChecked()) {
                        users.add(debt.getUser_id());
                    }
                }
                expense.setSubject(tiedSubject.getText().toString());
                expense.setDescription(tiedDescription.getText().toString());
                expense.setPrice(payForAdapter.getQuantity());
                if (expense.getId() == -1) {
                    if (validateInputs(users))
                        presenter.add(expense, users);
                } else {
                    if (validateInputs(users))
                        presenter.edit(expense, users);
                }
            }
        });
        tiedQuantity.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(5, 2)});
        tiedQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals(".")) {
                    s = "0.0";
                }
                updateQuantity(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private boolean validateInputs(List<Integer> users) {
        boolean valid = true;

        resetTextInputLayout();

        if (TextUtils.isEmpty(tiedDescription.getText().toString())) {
            tilDescription.setError(getString(R.string.err_empty));
            valid = false;
        }

        if (TextUtils.isEmpty(tiedQuantity.getText().toString())) {
            tilQuantity.setError(getString(R.string.err_empty));
            valid = false;
        }

        if (TextUtils.isEmpty(tiedSubject.getText().toString())) {
            tilSubject.setError(getString(R.string.err_empty));
            valid = false;
        }

        if (users.size() == 0){
            CommonUtils.showToast(getString(R.string.select_user));
            valid = true;
        }

        return valid;
    }

    private void resetTextInputLayout() {
        tilSubject.setError(null);
        tilQuantity.setError(null);
        tilDescription.setError(null);
    }

    private void updateQuantity(String quantity) {
        if (quantity.length() > 0) {
            payForAdapter.setQuantity(Float.parseFloat(quantity));
        } else {
            payForAdapter.setQuantity(0);
        }
    }

    @Override
    public void showUsers(List<Debt> debts) {
        payForAdapter.addAll(debts);
    }

    @Override
    public void showError(String error) {
        CommonUtils.showToast(error);
    }

    @Override
    public void onSuccess() {
        getActivity().onBackPressed();
    }

    @Override
    public void showError(int idError) {
        CommonUtils.showToast(getString(idError));
    }

    @Override
    public void setPresenter(ExpenseManageContract.Presenter presenter) {
        this.presenter = presenter;
    }
}

