package com.example.easyflat.ui.recoverpassword;

import com.example.easyflat.data.dao.connection.LoginDAO;
import com.example.easyflat.data.repository.RecoverPasswordRepository;

public class RecoverPasswordPresenter implements RecoverPasswordContract.Presenter, LoginDAO.ResponseRecoverPasswordApi {

    private RecoverPasswordActivity view;

    public RecoverPasswordPresenter(RecoverPasswordActivity view) {
        this.view = view;
    }

    @Override
    public void send(String email) {
        RecoverPasswordRepository.getRepository().sendRecoverPasswordMail(email, this);
    }

    @Override
    public void onSuccessSend(String message) {
        view.onSuccessSend(message);
    }

    @Override
    public void onErrorSend(String error) {
        view.onErrorSend(error);
    }
}
