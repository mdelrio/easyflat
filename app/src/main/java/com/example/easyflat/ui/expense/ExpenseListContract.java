package com.example.easyflat.ui.expense;

import com.example.easyflat.data.model.Month;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.ui.base.BaseListView;

import java.util.ArrayList;

public interface ExpenseListContract {
    interface View extends BaseListView<Presenter> {
        void showExpenses(ArrayList<Month> months);

        void showDebts(ArrayList<Debt> debts);

        void showError(String error);

        void onDeleteSuccess();
    }

    interface Presenter {
        void load();

        void delete(int expenseId);
    }
}


