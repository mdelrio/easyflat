package com.example.easyflat.ui.recoverpassword;

public interface RecoverPasswordContract {
    interface View{
        void onSuccessSend(String message);
        void onErrorSend(String error);
    }

    interface Presenter{
        void send(String email);
    }
}
