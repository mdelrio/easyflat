package com.example.easyflat.ui;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import com.example.easyflat.R;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.model.User;

import java.util.List;

public class EasyFlatApplication extends Application {

    public static final String CHANNEL_ID = "12";
    private static Context context;
    private static User userLogued;
    private static Group group;

    public static Context getAppContext(){
        return context;
    }

    public static User getUser(){
        return userLogued;
    }

    public static void setUser(User user){
        userLogued = user;
    }

    public static Group getGroup() {
        return group;
    }

    public static void setGroup(Group group) {
        EasyFlatApplication.group = group;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        createNotificationChannel();
        EasyFlatDatabase.getDatabase();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
