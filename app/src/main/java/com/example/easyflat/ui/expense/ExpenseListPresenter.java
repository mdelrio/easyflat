package com.example.easyflat.ui.expense;

import com.example.easyflat.data.dao.connection.DebtDAO;
import com.example.easyflat.data.dao.connection.ExpenseDAO;
import com.example.easyflat.data.model.Month;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.data.repository.DebtRepository;
import com.example.easyflat.data.repository.ExpenseRepository;
import com.example.easyflat.data.repository.UserRepository;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;
import com.github.mikephil.charting.formatter.IFillFormatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ExpenseListPresenter implements ExpenseListContract.Presenter, ExpenseDAO.ExpensesGetDAOListener, DebtDAO.GetDebtsListener {
    private ExpenseListContract.View view;

    public ExpenseListPresenter(ExpenseListContract.View view) {
        this.view = view;
    }

    @Override
    public void load() {
        view.showProgressBar();
        DebtRepository.getDebtRepository().deleteDebts();
        ExpenseRepository.getExpenseRepository().downloadExpenses(EasyFlatApplication.getGroup().getId(), this);
        }

    @Override
    public void delete(int expenseId) {
        ExpenseRepository.getExpenseRepository().deleteExpense(expenseId, this);
    }

    @Override
    public void onSuccessExpensesGet(List<Expense> expenses) {
        ExpenseRepository.getExpenseRepository().addAll(expenses);
        DebtRepository.getDebtRepository().downloadDebts(EasyFlatApplication.getGroup().getId(), this);
        ArrayList<Month> months = new ArrayList<>();
        months.add(new Month(Calendar.getInstance().get(Calendar.MONTH) + 1, Calendar.getInstance().get(Calendar.YEAR)));
        Calendar calendar = Calendar.getInstance();
        for (Expense expense : expenses) {
            try {
                calendar.setTime(CommonUtils.getSimpleDateFormat().parse(expense.getCreated_at()));
                if (calendar.get(Calendar.MONTH) != months.get(months.size() - 1).getMonth() - 1) {
                    months.add(new Month(calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR)));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            months.get(months.size() - 1).addExpense(expense);
        }
        view.hideProgressBar();
        view.showExpenses(months);
    }

    @Override
    public void onErrorExpensesGet(String error) {
        view.hideProgressBar();
        view.showError(error);
    }

    @Override
    public void onSuccessDelete(Expense expense) {
        view.onDeleteSuccess();
    }

    @Override
    public void onErrorDelete(String error) {
        view.showError(error);
    }

    @Override
    public void onSuccessGetDebts(List<Debt> debts) {
        DebtRepository.getDebtRepository().addAll(debts);
        ArrayList<Debt> debtsToShow = (ArrayList<Debt>) DebtRepository.getDebtRepository().getDebts();
        if (debtsToShow.size() > 0) {
            for (User user : UserRepository.getUserRepository().getUsers()) {
                if (debtsToShow.indexOf(new Debt(user.getId(), 0, 0)) == -1) {
                    debtsToShow.add(new Debt(user.getId(), -1, 0));
                } else {
                    debtsToShow.get(debtsToShow.indexOf(new Debt(user.getId(), 0, 0))).setName(user.getName());
                }
            }
        }
        view.hideProgressBar();
        view.showDebts(debtsToShow);
    }

    @Override
    public void onErrorGetDebts(String error) {
        view.hideProgressBar();
        view.showError(error);
    }
}

