package com.example.easyflat.ui.recoverpassword;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.example.easyflat.R;
import com.example.easyflat.data.utils.CommonUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class RecoverPasswordActivity extends AppCompatActivity implements RecoverPasswordContract.View {

    private Button btRecoverPassword;

    private TextInputLayout tilEmail;
    private TextInputEditText tiedEmail;

    private RecoverPasswordPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_password);

        btRecoverPassword = findViewById(R.id.btRevoverPassword);
        tilEmail = findViewById(R.id.tilRecoverPassword);
        tiedEmail = findViewById(R.id.tiedRecoverPassword);

        presenter = new RecoverPasswordPresenter(this);

        btRecoverPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifyData())
                presenter.send(tiedEmail.getText().toString());
            }
        });
    }

    private boolean verifyData() {
        boolean valid = true;

        tilEmail.setError(null);

        if (!tiedEmail.getText().toString().matches(CommonUtils.REGEX_EMAIL)){
            tilEmail.setError(getString(R.string.err_email_not_valid));
            valid = false;
        }
        if (TextUtils.isEmpty(tiedEmail.getText().toString())){
            tilEmail.setError(getString(R.string.err_email_empty));
            valid = false;
        }
        return valid;
    }

    @Override
    public void onSuccessSend(String message) {
        CommonUtils.showToast(message);
        finish();
    }

    @Override
    public void onErrorSend(String error) {
        CommonUtils.showToast(error);
    }

}
