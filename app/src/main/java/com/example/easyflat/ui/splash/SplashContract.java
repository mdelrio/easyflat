package com.example.easyflat.ui.splash;

public interface SplashContract {
    interface View{
        void onUserExists();
        void notUserExists();
        void onGroupsExists();
        void notGroupsExists();
    }

    interface Presenter{
        void checkPreferences();
        void checkGroups();
    }
}
