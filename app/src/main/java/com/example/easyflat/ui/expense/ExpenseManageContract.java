package com.example.easyflat.ui.expense;

import androidx.annotation.Nullable;

import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.debt.Debt;
import com.example.easyflat.data.model.expense.Expense;
import com.example.easyflat.ui.base.BaseManageView;

import java.util.List;

public interface ExpenseManageContract {
    interface View extends BaseManageView<Presenter> {

        void showUsers(List<Debt> debts);

        void showError(String error);
    }

    interface Presenter {
        void add(Expense expense, List<Integer> users);
        void edit(Expense expense, List<Integer> users);
        void load(@Nullable int expenseId);
    }
}


