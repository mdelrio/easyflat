package com.example.easyflat.ui.navigationdrawer;

import com.example.easyflat.R;
import com.example.easyflat.data.dao.connection.GroupDAO;
import com.example.easyflat.data.dao.connection.UserDAO;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.repository.GroupRepository;
import com.example.easyflat.data.repository.NavigationDrawerRepository;
import com.example.easyflat.data.repository.UserRepository;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.List;

public class NavigationDrawerPresenter implements NavigationDrawerContract.Presenter, GroupDAO.ExitGroupDAOListener, UserDAO.ResponseListUsersApi {

    private NavigationDrawerActivity view;

    public NavigationDrawerPresenter(NavigationDrawerActivity view) {
        this.view = view;
    }

    @Override
    public void loadGroups() {
        view.showGroups(GroupRepository.getGroupRepository().getGroups());
    }

    @Override
    public void clearDatabase() {
        NavigationDrawerRepository.getNavigationDrawerRepository().clearAllDatabase();
        CommonUtils.clearSharedPreferences(EasyFlatApplication.getAppContext().getString(R.string.shared_preferences_login));
        view.onSuccessClearDatabase();
    }

    @Override
    public void changeGroup(Group group) {
        EasyFlatApplication.setGroup(group);
        UserRepository.getUserRepository().deleteAll();
        UserRepository.getUserRepository().downloadUsers(EasyFlatApplication.getGroup().getId(), this);
    }

    @Override
    public void exitGroup(Group group) {
        GroupRepository.getGroupRepository().desuscribeGroup(group.getId(), this);
    }

    @Override
    public void checkGroups() {
        if (GroupRepository.getGroupRepository().getGroups().size() != 0) {
            EasyFlatApplication.setGroup(GroupRepository.getGroupRepository().getGroups().get(0));
            view.onGroupExists();
        }
        else
            view.onNotGroupExists();
    }

    @Override
    public void onSuccessExit(int id) {
        GroupRepository.getGroupRepository().deleteGroup(id);
        view.onSuccessExit();
    }

    @Override
    public void onErrorExit(String error) {
    }

    @Override
    public void onSuccessObtained(List<User> users) {
        UserRepository.getUserRepository().addAll(users);
        view.onSuccessGroupChange();
    }

    @Override
    public void onError(String error) {
        view.onSuccessClearDatabase();
    }
}
