package com.example.easyflat.ui.login;

import com.example.easyflat.R;
import com.example.easyflat.data.dao.connection.GroupDAO;
import com.example.easyflat.data.dao.connection.UserDAO;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.repository.GroupRepository;
import com.example.easyflat.data.repository.NavigationDrawerRepository;
import com.example.easyflat.data.repository.UserRepository;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.List;

public class LoginPresenter implements LoginContract.Presenter, UserRepository.RepositorySignInListener, UserRepository.RepositoryGetUserListener, GroupDAO.GetGroupsDAOListener, UserDAO.ResponseListUsersApi {
    private LoginContract.View view;
    private Boolean remember;

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void userLogin(String email, String password, Boolean remember) {
        this.remember = remember;
        UserRepository.getUserRepository().signInServer(email, password, this);
    }

    @Override
    public void checkGroups() {
        GroupRepository.getGroupRepository().downloadGroups(this);
    }

    private void saveSharedPreferences(String token) {
        CommonUtils.saveSharedPreference(EasyFlatApplication.getAppContext().getString(R.string.shared_preferences_login), CommonUtils.KEY_TOKEN, token);
    }

    @Override
    public void onErrorLogin(String error) {
        view.onErrorLogin();
    }

    @Override
    public void onSuccessSignIn(String token) {
        saveSharedPreferences(token);
        UserRepository.getUserRepository().getUser(this);
    }

    @Override
    public void onSuccessGetUser(User user) {
        EasyFlatApplication.setUser(user);
        UserRepository.getUserRepository().add(user);
        view.onSuccessLogin();
    }

    @Override
    public void onErrorGetUser(String error) {
        view.onErrorLogin();
        CommonUtils.showToast(error);
    }

    @Override
    public void onSuccessGroupsGet(List<Group> groups) {
        GroupRepository.getGroupRepository().addAll(groups);
        if (groups.size() != 0){
            EasyFlatApplication.setGroup(groups.get(0));
            UserRepository.getUserRepository().downloadUsers(groups.get(0).getId(), this);
        }
        else
            view.onGroupNotExists();
    }

    @Override
    public void onErrorGroupsGet(String error) {

    }

    @Override
    public void onSuccessObtained(List<User> users) {
        UserRepository.getUserRepository().addAll(users);
        view.onGroupsExists();
    }

    @Override
    public void onError(String error) {

    }
}
