package com.example.easyflat.ui.signup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.easyflat.R;
import com.example.easyflat.data.model.User;
import com.example.easyflat.data.utils.CommonUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class SignUpActivity extends AppCompatActivity implements SignUpContract.View {

    private Button btLogin;
    private Button btSignUp;

    private TextInputLayout tilName;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText tiedName;
    private TextInputEditText tiedEmail;
    private TextInputEditText tiedPassword;
    private SignUpContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        btLogin = findViewById(R.id.btSignUpLogin);
        btSignUp = findViewById(R.id.btSignUpSignUp);

        tilName = findViewById(R.id.tilSignUpName);
        tilEmail = findViewById(R.id.tilSignUpEmail);
        tilPassword = findViewById(R.id.tilSignUpPassword);

        tiedName = findViewById(R.id.tiedSignUpName);
        tiedEmail = findViewById(R.id.tiedSignUpEmail);
        tiedPassword = findViewById(R.id.tiedSignUpPassword);

        presenter = new SignUpPresenter(this);

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                Animatoo.animateSwipeRight(SignUpActivity.this);
            }
        });

        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid())
                    presenter.add(tiedName.getText().toString(), tiedEmail.getText().toString(), tiedPassword.getText().toString());
            }
        });
    }

    private boolean isValid(){
        boolean valid = true;

        resetTextInputLayout();

        if (!tiedEmail.getText().toString().matches(CommonUtils.REGEX_EMAIL)){
            tilEmail.setError(getString(R.string.err_email_not_valid));
            valid = false;
        }

        if (!tiedPassword.getText().toString().matches(CommonUtils.REGEX_PASSWORD)){
            tilPassword.setError(getString(R.string.err_password_not_valid));
            valid = false;
        }

        if (TextUtils.isEmpty(tiedName.getText().toString())) {
            tilName.setError(getString(R.string.err_name_empty));
            valid = false;
        }

        if (TextUtils.isEmpty(tiedPassword.getText().toString())){
            tilPassword.setError(getString(R.string.err_password_empty));
            valid = false;
        }

        if (TextUtils.isEmpty(tiedEmail.getText().toString())){
            tilEmail.setError(getString(R.string.err_email_empty));
            valid = false;
        }
        return valid;
    }

    private void resetTextInputLayout() {
        tilName.setError(null);
        tilEmail.setError(null);
        tilPassword.setError(null);
    }

    @Override
    public void onSuccess() {
        CommonUtils.showToast(getString(R.string.sign_up_successfull));
        onBackPressed();
    }

    @Override
    public void userExists() {
        resetTextInputLayout();
        tilEmail.setError(getString(R.string.err_email_exists));
    }
}
