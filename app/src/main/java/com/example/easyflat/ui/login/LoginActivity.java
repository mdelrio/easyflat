package com.example.easyflat.ui.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.easyflat.R;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.navigationdrawer.NavigationDrawerActivity;
import com.example.easyflat.ui.recoverpassword.RecoverPasswordActivity;
import com.example.easyflat.ui.group.GroupActivity;
import com.example.easyflat.ui.signup.SignUpActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    private Button btSignIn;
    private Button btSignUp;
    private Button btRecoverPassword;
    private ImageView ivLoginGoogle;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText tiedEmail;
    private TextInputEditText tiedPassword;
    private CheckBox cbLoginRemember;
    private LoginContract.Presenter presenter;
    Intent intent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        final GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        btSignIn = findViewById(R.id.btLoginSignIn);
        btSignUp = findViewById(R.id.btLoginSignUp);
        btRecoverPassword = findViewById(R.id.btLoginRecoverPassword);
        ivLoginGoogle = findViewById(R.id.ivLoginGoogle);

        tilEmail = findViewById(R.id.tilLoginEmail);
        tilPassword = findViewById(R.id.tilLoginPassword);
        tiedEmail = findViewById(R.id.tiedLoginEmail);
        tiedPassword = findViewById(R.id.tiedLoginPassword);

        cbLoginRemember = findViewById(R.id.cbLoginRemember);

        presenter = new LoginPresenter(this);

        btSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid())
                    presenter.userLogin(tiedEmail.getText().toString(), tiedPassword.getText().toString(), cbLoginRemember.isChecked());
            }
        });

        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                Animatoo.animateSwipeLeft(LoginActivity.this);
            }
        });

        btRecoverPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(LoginActivity.this, RecoverPasswordActivity.class);
                startActivity(intent);
                Animatoo.animateCard(LoginActivity.this);
            }
        });

        ivLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = mGoogleSignInClient.getSignInIntent();
                startActivity(intent);
            }
        });
    }

    private boolean isValid(){
        boolean valid = true;

        resetTextInputLayout();

        if (!tiedEmail.getText().toString().matches(CommonUtils.REGEX_EMAIL)){
            tilEmail.setError(getString(R.string.err_email_not_valid));
            valid = false;
        }

        if (TextUtils.isEmpty(tiedPassword.getText().toString())){
            tilPassword.setError(getString(R.string.err_password_empty));
            valid = false;
        }

        if (TextUtils.isEmpty(tiedEmail.getText().toString())){
            tilEmail.setError(getString(R.string.err_email_empty));
            valid = false;
        }
        return valid;
    }

    private void resetTextInputLayout() {
        tilEmail.setError(null);
        tilPassword.setError(null);
    }

    @Override
    public void onSuccessLogin() {
       presenter.checkGroups();
    }

    @Override
    public void onErrorLogin() {
        CommonUtils.showToast(getString(R.string.err_login));
    }

    @Override
    public void onGroupsExists() {
        intent = new Intent(LoginActivity.this, NavigationDrawerActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onGroupNotExists() {
        intent = new Intent(LoginActivity.this, GroupActivity.class);
        startActivity(intent);
        finish();
    }
}
