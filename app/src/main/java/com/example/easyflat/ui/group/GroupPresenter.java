package com.example.easyflat.ui.group;

import com.example.easyflat.data.dao.connection.GroupDAO;
import com.example.easyflat.data.model.group.Group;
import com.example.easyflat.data.repository.GroupRepository;
import com.example.easyflat.ui.EasyFlatApplication;

public class GroupPresenter implements GroupContract.Presenter, GroupDAO.CreateGroupDAOListener, GroupDAO.JoinGroupDAOListener {

    private GroupContract.View view;

    public GroupPresenter(GroupContract.View view) {
        this.view = view;
    }

    @Override
    public void createGroup(String name) {
        GroupRepository.getGroupRepository().createGroup(name, this);
    }

    @Override
    public void joinGroup(String code) {
        GroupRepository.getGroupRepository().joinGroup(code, this);
    }

    @Override
    public void onSuccessCreated(Group group) {
        GroupRepository.getGroupRepository().addGroup(group);
        EasyFlatApplication.setGroup(group);
        view.onSuccessCreated();
    }

    @Override
    public void onErrorCreated(String error) {

    }

    @Override
    public void onSuccessJoined(Group group) {
        GroupRepository.getGroupRepository().addGroup(group);
        EasyFlatApplication.setGroup(group);
        view.onSuccessJoined();
    }

    @Override
    public void onErrorJoined(String error) {
    }
}
