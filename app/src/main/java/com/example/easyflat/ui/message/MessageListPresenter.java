package com.example.easyflat.ui.message;

import com.example.easyflat.data.dao.connection.MessageDAO;
import com.example.easyflat.data.dao.room.EasyFlatDatabase;
import com.example.easyflat.data.model.message.Message;
import com.example.easyflat.data.repository.MessageRepository;
import com.example.easyflat.ui.EasyFlatApplication;

import java.util.List;

public class MessageListPresenter implements MessageListContract.Presenter, MessageDAO.GetMessagesDAOListener {

    private MessageListContract.View view;

    public MessageListPresenter(MessageListContract.View view) {
        this.view = view;
    }

    @Override
    public void load() {
        view.showProgressBar();
        MessageRepository.getRepository().getAll(EasyFlatApplication.getGroup().getId(), this);
    }

    @Override
    public void sendMessage(String text) {
        MessageRepository.getRepository().sendMessage(text, EasyFlatApplication.getGroup().getId(), EasyFlatApplication.getUser().getId(), this);
    }

    @Override
    public void onSuccesMessagesGet(List<Message> messages) {
        MessageRepository.getRepository().addAll(messages);
        view.hideProgressBar();
        view.showListMessages(messages);
    }

    @Override
    public void onErrorMessagesGet(String error) {
        view.hideProgressBar();
    }

    @Override
    public void onSuccesMessageGet(Message message) {
    }

    @Override
    public void onErrorMessageGet(String error) {

    }

    @Override
    public void onSuccessSendMessage(Message message) {
        MessageRepository.getRepository().addMessage(message);
        view.onSuccessAdd(message);
    }

    @Override
    public void onErrorSendMessage(String message) {

    }
}
