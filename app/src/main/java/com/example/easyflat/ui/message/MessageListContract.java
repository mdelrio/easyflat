package com.example.easyflat.ui.message;

import com.example.easyflat.data.model.message.Message;
import com.example.easyflat.ui.base.BaseListView;

import java.util.List;

public interface MessageListContract {
    interface View extends BaseListView<Presenter> {
        void showListMessages(List<Message> messages);
        void onSuccessAdd(Message message);
        void onErrorAdd();
    }

    interface Presenter{
        void load();
        void sendMessage(String text);
    }
}
