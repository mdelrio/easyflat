package com.example.easyflat.ui.group;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.example.easyflat.R;
import com.example.easyflat.data.utils.CommonUtils;
import com.example.easyflat.ui.navigationdrawer.NavigationDrawerActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class GroupActivity extends AppCompatActivity implements GroupContract.View {

    private GroupContract.Presenter presenter;

    private TextInputLayout tilCode;
    private TextInputEditText tiedCode;
    private TextInputLayout tilName;
    private TextInputEditText tiedName;
    private Button btCreateGroup;
    private Button btJoinGroup;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        tilCode = findViewById(R.id.tilGroupCodeGroup);
        tiedCode = findViewById(R.id.tiedGroupCodeGroup);
        tilName = findViewById(R.id.tilGroupNameGroup);
        tiedName = findViewById(R.id.tiedGroupNameGroup);
        btCreateGroup = findViewById(R.id.btGroupCreateGroup);
        btJoinGroup = findViewById(R.id.btGroupJoinGroup);

        presenter = new GroupPresenter(this);

        btCreateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateData(tilName, tiedName))
                    presenter.createGroup(tiedName.getText().toString());
            }
        });

        btJoinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateData(tilCode, tiedCode))
                    presenter.joinGroup(tiedCode.getText().toString());
            }
        });
    }

    private boolean validateData(TextInputLayout textInputLayout, TextInputEditText textInputEditText) {
        boolean valid = true;

        textInputLayout.setError(null);

        if (TextUtils.isEmpty(textInputEditText.getText().toString())) {
            textInputLayout.setError(getString(R.string.err_empty));
            valid = false;
        }
        return valid;
    }

    @Override
    public void onSuccessCreated() {
        intent = new Intent(this, NavigationDrawerActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onErrorCreated(String message) {
        tilName.setError(message);
    }

    @Override
    public void onSuccessJoined() {
        intent = new Intent(this, NavigationDrawerActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onErrorJoined(String message) {
        tilCode.setError(message);
    }
}
