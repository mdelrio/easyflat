package com.example.easyflat.ui.login;

import com.example.easyflat.data.model.User;

public interface LoginContract {
    interface View{
        void onSuccessLogin();
        void onErrorLogin();
        void onGroupsExists();
        void onGroupNotExists();
    }

    interface Presenter{
        void userLogin(String email, String password, Boolean remember);
        void checkGroups();
    }
}
