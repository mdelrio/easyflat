package com.example.easyflat.ui.navigationdrawer;

import com.example.easyflat.data.model.group.Group;

import java.util.List;

public interface NavigationDrawerContract {
    interface View{
        void showGroups(List<Group> groups);
        void onSuccessClearDatabase();
        void onSuccessGroupChange();
        void onSuccessExit();
        void onGroupExists();
        void onNotGroupExists();
    }

    interface Presenter{
        void loadGroups();
        void clearDatabase();
        void changeGroup(Group group);
        void exitGroup(Group group);
        void checkGroups();
    }
}
